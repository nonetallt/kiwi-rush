info face=font size=60 bold=0 italic=0 charset= unicode= stretchH=100 smooth=1 aa=1 padding=2,2,2,2 spacing=0,0 outline=0
common lineHeight=67 base=47 scaleW=325 scaleH=512 pages=1 packed=0
page id=0 file="font.png"
chars count=95
char id=32 x=2 y=2 width=0 height=0 xoffset=0 yoffset=47 xadvance=17 page=0 chnl=15
char id=33 x=2 y=4 width=9 height=45 xoffset=5 yoffset=4 xadvance=17 page=0 chnl=15
char id=34 x=2 y=51 width=18 height=18 xoffset=3 yoffset=4 xadvance=21 page=0 chnl=15
char id=35 x=13 y=2 width=34 height=47 xoffset=1 yoffset=3 xadvance=33 page=0 chnl=15
char id=36 x=2 y=71 width=31 height=55 xoffset=2 yoffset=0 xadvance=33 page=0 chnl=15
char id=37 x=2 y=128 width=49 height=48 xoffset=3 yoffset=3 xadvance=53 page=0 chnl=15
char id=38 x=35 y=51 width=38 height=47 xoffset=3 yoffset=3 xadvance=40 page=0 chnl=15
char id=39 x=22 y=51 width=8 height=17 xoffset=3 yoffset=4 xadvance=11 page=0 chnl=15
char id=40 x=2 y=178 width=17 height=58 xoffset=4 yoffset=3 xadvance=20 page=0 chnl=15
char id=41 x=2 y=238 width=17 height=58 xoffset=4 yoffset=3 xadvance=20 page=0 chnl=15
char id=42 x=35 y=100 width=22 height=21 xoffset=2 yoffset=3 xadvance=23 page=0 chnl=15
char id=43 x=49 y=2 width=31 height=31 xoffset=3 yoffset=12 xadvance=35 page=0 chnl=15
char id=44 x=2 y=298 width=9 height=17 xoffset=5 yoffset=41 xadvance=17 page=0 chnl=15
char id=45 x=49 y=35 width=18 height=7 xoffset=2 yoffset=29 xadvance=20 page=0 chnl=15
char id=46 x=69 y=35 width=8 height=8 xoffset=5 yoffset=41 xadvance=17 page=0 chnl=15
char id=47 x=2 y=317 width=19 height=47 xoffset=0 yoffset=3 xadvance=17 page=0 chnl=15
char id=48 x=21 y=178 width=30 height=46 xoffset=2 yoffset=4 xadvance=33 page=0 chnl=15
char id=49 x=21 y=226 width=18 height=45 xoffset=7 yoffset=4 xadvance=33 page=0 chnl=15
char id=50 x=2 y=366 width=31 height=45 xoffset=2 yoffset=4 xadvance=33 page=0 chnl=15
char id=51 x=23 y=273 width=30 height=46 xoffset=3 yoffset=4 xadvance=33 page=0 chnl=15
char id=52 x=41 y=226 width=32 height=45 xoffset=1 yoffset=4 xadvance=33 page=0 chnl=15
char id=53 x=2 y=413 width=31 height=45 xoffset=2 yoffset=5 xadvance=33 page=0 chnl=15
char id=54 x=2 y=460 width=31 height=46 xoffset=2 yoffset=4 xadvance=33 page=0 chnl=15
char id=55 x=53 y=123 width=30 height=45 xoffset=3 yoffset=5 xadvance=33 page=0 chnl=15
char id=56 x=53 y=170 width=31 height=46 xoffset=2 yoffset=4 xadvance=33 page=0 chnl=15
char id=57 x=75 y=45 width=31 height=46 xoffset=2 yoffset=4 xadvance=33 page=0 chnl=15
char id=63 x=85 y=93 width=30 height=46 xoffset=3 yoffset=3 xadvance=33 page=0 chnl=15
char id=58 x=82 y=2 width=8 height=33 xoffset=5 yoffset=16 xadvance=17 page=0 chnl=15
char id=59 x=23 y=321 width=9 height=42 xoffset=5 yoffset=16 xadvance=17 page=0 chnl=15
char id=60 x=92 y=2 width=31 height=31 xoffset=3 yoffset=11 xadvance=35 page=0 chnl=15
char id=61 x=85 y=141 width=31 height=20 xoffset=3 yoffset=17 xadvance=35 page=0 chnl=15
char id=62 x=34 y=321 width=31 height=32 xoffset=3 yoffset=11 xadvance=35 page=0 chnl=15
char id=64 x=117 y=35 width=58 height=59 xoffset=3 yoffset=3 xadvance=61 page=0 chnl=15
char id=65 x=55 y=273 width=43 height=45 xoffset=0 yoffset=4 xadvance=40 page=0 chnl=15
char id=66 x=75 y=218 width=35 height=45 xoffset=4 yoffset=4 xadvance=40 page=0 chnl=15
char id=67 x=86 y=163 width=40 height=47 xoffset=3 yoffset=3 xadvance=43 page=0 chnl=15
char id=68 x=118 y=96 width=38 height=45 xoffset=5 yoffset=4 xadvance=43 page=0 chnl=15
char id=69 x=35 y=355 width=34 height=45 xoffset=5 yoffset=4 xadvance=40 page=0 chnl=15
char id=70 x=35 y=402 width=31 height=45 xoffset=5 yoffset=4 xadvance=37 page=0 chnl=15
char id=71 x=35 y=449 width=42 height=47 xoffset=3 yoffset=3 xadvance=47 page=0 chnl=15
char id=72 x=68 y=402 width=36 height=45 xoffset=5 yoffset=4 xadvance=43 page=0 chnl=15
char id=73 x=79 y=449 width=8 height=45 xoffset=6 yoffset=4 xadvance=17 page=0 chnl=15
char id=74 x=89 y=449 width=26 height=46 xoffset=2 yoffset=4 xadvance=30 page=0 chnl=15
char id=75 x=71 y=320 width=38 height=45 xoffset=4 yoffset=4 xadvance=40 page=0 chnl=15
char id=76 x=100 y=265 width=29 height=45 xoffset=4 yoffset=4 xadvance=33 page=0 chnl=15
char id=77 x=112 y=212 width=43 height=45 xoffset=4 yoffset=4 xadvance=50 page=0 chnl=15
char id=78 x=128 y=143 width=36 height=45 xoffset=5 yoffset=4 xadvance=43 page=0 chnl=15
char id=79 x=106 y=367 width=43 height=47 xoffset=3 yoffset=3 xadvance=47 page=0 chnl=15
char id=80 x=158 y=96 width=35 height=45 xoffset=5 yoffset=4 xadvance=40 page=0 chnl=15
char id=81 x=111 y=312 width=45 height=49 xoffset=3 yoffset=3 xadvance=47 page=0 chnl=15
char id=82 x=131 y=259 width=40 height=45 xoffset=5 yoffset=4 xadvance=43 page=0 chnl=15
char id=83 x=157 y=190 width=37 height=47 xoffset=3 yoffset=3 xadvance=40 page=0 chnl=15
char id=84 x=166 y=143 width=36 height=45 xoffset=1 yoffset=4 xadvance=37 page=0 chnl=15
char id=85 x=177 y=2 width=36 height=46 xoffset=5 yoffset=4 xadvance=43 page=0 chnl=15
char id=86 x=195 y=50 width=42 height=45 xoffset=0 yoffset=4 xadvance=40 page=0 chnl=15
char id=87 x=215 y=2 width=58 height=45 xoffset=1 yoffset=4 xadvance=57 page=0 chnl=15
char id=88 x=117 y=416 width=42 height=45 xoffset=0 yoffset=4 xadvance=40 page=0 chnl=15
char id=89 x=117 y=463 width=42 height=45 xoffset=0 yoffset=4 xadvance=40 page=0 chnl=15
char id=90 x=151 y=363 width=36 height=45 xoffset=1 yoffset=4 xadvance=37 page=0 chnl=15
char id=91 x=161 y=410 width=14 height=57 xoffset=4 yoffset=4 xadvance=17 page=0 chnl=15
char id=92 x=158 y=306 width=19 height=47 xoffset=0 yoffset=3 xadvance=17 page=0 chnl=15
char id=93 x=173 y=239 width=14 height=57 xoffset=1 yoffset=4 xadvance=17 page=0 chnl=15
char id=94 x=71 y=367 width=28 height=26 xoffset=2 yoffset=3 xadvance=28 page=0 chnl=15
char id=95 x=173 y=298 width=37 height=6 xoffset=-1 yoffset=55 xadvance=33 page=0 chnl=15
char id=96 x=157 y=239 width=14 height=10 xoffset=3 yoffset=4 xadvance=20 page=0 chnl=15
char id=97 x=161 y=469 width=31 height=35 xoffset=2 yoffset=15 xadvance=33 page=0 chnl=15
char id=98 x=179 y=306 width=29 height=46 xoffset=4 yoffset=4 xadvance=33 page=0 chnl=15
char id=99 x=195 y=97 width=29 height=35 xoffset=2 yoffset=15 xadvance=30 page=0 chnl=15
char id=100 x=189 y=239 width=29 height=46 xoffset=2 yoffset=4 xadvance=33 page=0 chnl=15
char id=101 x=196 y=190 width=31 height=35 xoffset=2 yoffset=15 xadvance=33 page=0 chnl=15
char id=102 x=204 y=134 width=21 height=46 xoffset=1 yoffset=3 xadvance=17 page=0 chnl=15
char id=103 x=177 y=410 width=30 height=47 xoffset=2 yoffset=15 xadvance=33 page=0 chnl=15
char id=104 x=194 y=459 width=28 height=45 xoffset=4 yoffset=4 xadvance=33 page=0 chnl=15
char id=105 x=108 y=35 width=7 height=45 xoffset=4 yoffset=4 xadvance=13 page=0 chnl=15
char id=106 x=227 y=97 width=14 height=58 xoffset=-3 yoffset=4 xadvance=13 page=0 chnl=15
char id=107 x=239 y=49 width=28 height=45 xoffset=4 yoffset=4 xadvance=30 page=0 chnl=15
char id=108 x=189 y=354 width=7 height=45 xoffset=4 yoffset=4 xadvance=13 page=0 chnl=15
char id=109 x=198 y=354 width=45 height=34 xoffset=4 yoffset=15 xadvance=50 page=0 chnl=15
char id=110 x=210 y=306 width=28 height=34 xoffset=4 yoffset=15 xadvance=33 page=0 chnl=15
char id=111 x=209 y=390 width=32 height=35 xoffset=2 yoffset=15 xadvance=33 page=0 chnl=15
char id=112 x=220 y=227 width=29 height=46 xoffset=4 yoffset=15 xadvance=33 page=0 chnl=15
char id=113 x=229 y=157 width=29 height=46 xoffset=2 yoffset=15 xadvance=33 page=0 chnl=15
char id=114 x=243 y=96 width=19 height=34 xoffset=4 yoffset=15 xadvance=20 page=0 chnl=15
char id=115 x=240 y=275 width=28 height=35 xoffset=2 yoffset=15 xadvance=30 page=0 chnl=15
char id=116 x=251 y=205 width=18 height=45 xoffset=1 yoffset=5 xadvance=17 page=0 chnl=15
char id=117 x=240 y=312 width=27 height=34 xoffset=4 yoffset=16 xadvance=33 page=0 chnl=15
char id=118 x=260 y=132 width=31 height=33 xoffset=1 yoffset=16 xadvance=30 page=0 chnl=15
char id=119 x=264 y=96 width=45 height=33 xoffset=0 yoffset=16 xadvance=43 page=0 chnl=15
char id=120 x=260 y=167 width=32 height=33 xoffset=0 yoffset=16 xadvance=30 page=0 chnl=15
char id=121 x=224 y=427 width=31 height=46 xoffset=1 yoffset=16 xadvance=30 page=0 chnl=15
char id=122 x=293 y=131 width=30 height=33 xoffset=1 yoffset=16 xadvance=30 page=0 chnl=15
char id=123 x=245 y=348 width=19 height=59 xoffset=2 yoffset=3 xadvance=20 page=0 chnl=15
char id=124 x=275 y=2 width=7 height=59 xoffset=6 yoffset=3 xadvance=16 page=0 chnl=15
char id=125 x=284 y=2 width=19 height=59 xoffset=1 yoffset=3 xadvance=20 page=0 chnl=15
char id=126 x=35 y=498 width=32 height=12 xoffset=3 yoffset=21 xadvance=35 page=0 chnl=15
char id=32 x=0 y=0 width=0 height=0 xoffset=3 yoffset=21 xadvance=17 page=0 chnl=15
kernings count=97
kerning first=32 second=65 amount=-3
kerning first=32 second=84 amount=-1
kerning first=32 second=89 amount=-1
kerning first=49 second=49 amount=-4
kerning first=65 second=32 amount=-3
kerning first=65 second=84 amount=-4
kerning first=65 second=86 amount=-4
kerning first=65 second=87 amount=-2
kerning first=65 second=89 amount=-4
kerning first=65 second=118 amount=-1
kerning first=65 second=119 amount=-1
kerning first=65 second=121 amount=-1
kerning first=70 second=44 amount=-7
kerning first=70 second=46 amount=-7
kerning first=70 second=65 amount=-3
kerning first=76 second=32 amount=-2
kerning first=76 second=84 amount=-4
kerning first=76 second=86 amount=-4
kerning first=76 second=87 amount=-4
kerning first=76 second=89 amount=-4
kerning first=76 second=121 amount=-2
kerning first=80 second=32 amount=-1
kerning first=80 second=44 amount=-8
kerning first=80 second=46 amount=-8
kerning first=80 second=65 amount=-4
kerning first=82 second=84 amount=-1
kerning first=82 second=86 amount=-1
kerning first=82 second=87 amount=-1
kerning first=82 second=89 amount=-1
kerning first=84 second=32 amount=-1
kerning first=84 second=44 amount=-7
kerning first=84 second=45 amount=-3
kerning first=84 second=46 amount=-7
kerning first=84 second=58 amount=-7
kerning first=84 second=59 amount=-7
kerning first=84 second=65 amount=-4
kerning first=84 second=79 amount=-1
kerning first=84 second=97 amount=-7
kerning first=84 second=99 amount=-7
kerning first=84 second=101 amount=-7
kerning first=84 second=105 amount=-2
kerning first=84 second=111 amount=-7
kerning first=84 second=114 amount=-2
kerning first=84 second=115 amount=-7
kerning first=84 second=117 amount=-2
kerning first=84 second=119 amount=-3
kerning first=84 second=121 amount=-3
kerning first=86 second=44 amount=-6
kerning first=86 second=45 amount=-3
kerning first=86 second=46 amount=-6
kerning first=86 second=58 amount=-2
kerning first=86 second=59 amount=-2
kerning first=86 second=65 amount=-4
kerning first=86 second=97 amount=-4
kerning first=86 second=101 amount=-3
kerning first=86 second=105 amount=-1
kerning first=86 second=111 amount=-3
kerning first=86 second=114 amount=-2
kerning first=86 second=117 amount=-2
kerning first=86 second=121 amount=-2
kerning first=87 second=44 amount=-3
kerning first=87 second=45 amount=-1
kerning first=87 second=46 amount=-3
kerning first=87 second=58 amount=-1
kerning first=87 second=59 amount=-1
kerning first=87 second=65 amount=-2
kerning first=87 second=97 amount=-2
kerning first=87 second=101 amount=-1
kerning first=87 second=105 amount=0
kerning first=87 second=111 amount=-1
kerning first=87 second=114 amount=-1
kerning first=87 second=117 amount=-1
kerning first=87 second=121 amount=-1
kerning first=89 second=32 amount=-1
kerning first=89 second=44 amount=-8
kerning first=89 second=45 amount=-6
kerning first=89 second=46 amount=-8
kerning first=89 second=58 amount=-3
kerning first=89 second=59 amount=-4
kerning first=89 second=65 amount=-4
kerning first=89 second=97 amount=-4
kerning first=89 second=101 amount=-6
kerning first=89 second=105 amount=-2
kerning first=89 second=111 amount=-6
kerning first=89 second=112 amount=-4
kerning first=89 second=113 amount=-6
kerning first=89 second=117 amount=-3
kerning first=89 second=118 amount=-3
kerning first=102 second=102 amount=-1
kerning first=114 second=44 amount=-3
kerning first=114 second=46 amount=-3
kerning first=118 second=44 amount=-4
kerning first=118 second=46 amount=-4
kerning first=119 second=44 amount=-3
kerning first=119 second=46 amount=-3
kerning first=121 second=44 amount=-4
kerning first=121 second=46 amount=-4