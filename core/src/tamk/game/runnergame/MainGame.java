
package tamk.game.runnergame;

import com.badlogic.gdx.Game;
import tamk.game.runnergame.screens.GameScreen;
import tamk.game.runnergame.screens.MenuScreen;

/**
 * MainGame initializes the settings class.
 *
 * @author Overhill Productions
 * @version 1.2.1
 * @since 2015-0309
 */

public class MainGame extends Game {

	// settings
	private Settings settings;

	@Override
	public void create () {
		settings = new Settings(this);
	}

	@Override
	public void render () {
		super.render();
	}
}
