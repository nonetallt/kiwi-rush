
package tamk.game.runnergame;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Timer;
import tamk.game.runnergame.levels.LevelData;
import tamk.game.runnergame.objects.GameObject;
import tamk.game.runnergame.objects.Player;
import tamk.game.runnergame.screens.GameScreen;
import tamk.game.runnergame.screens.HighscoreScreen;
import tamk.game.runnergame.screens.MenuScreen;
import tamk.game.runnergame.screens.TutorialScreen;
import tamk.game.runnergame.timerTasks.ObjectCollisionCheckerTask;
import tamk.game.runnergame.timerTasks.RemoveMarkedObjectsTask;
import tamk.game.runnergame.timerTasks.TestObjectSpawnerTask;
import tamk.game.runnergame.utilities.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import tamk.game.runnergame.utilities.InputDelayManager;
import tamk.game.runnergame.utilities.LevelReader;
import tamk.game.runnergame.utilities.TextInput;

import java.util.ArrayList;

/**
 * Contains various tools and utilities needed for the game mechanics.
 *
 * @author Overhill Productions
 * @version 1.2.1
 * @since 2015-0309
 */


public class Settings {

    // world
    private static final float worldWidth = 480f;
    private static final float worldHeight = 800f;
    private Game game;
    private SpriteBatch batch;

    // screens
    private GameScreen gameScreen;
    private MenuScreen menuScreen;
    private TutorialScreen tutorialScreen;
    private HighscoreScreen highscoreScreen;

    // levels
    private LevelReader levelReader;
    private LevelData currentLevel;

    // objects
    private ArrayList<GameObject> objects;
    private Player player;
    private AssetManager assetManager;
    private float speed;

    // timer
    private Timer removeMarkedObjectsTimer;
    private Timer spawmObstacleTimer;
    private Timer checkCollisionsTimer;
    private ObjectCollisionCheckerTask collisionTask;
    private RemoveMarkedObjectsTask objectRemovalTask;
    private TestObjectSpawnerTask objectSpawnerTask;

    // math data
    private String[] mathData;
    private int uiData;
    private int combo;

    // touch detection
    private Vector3 touchPosition;
    private OrthographicCamera uiCamera;

    // preferences & scores
    private Preferences prefs;
    private TextInput listener;

    // constructor
    public Settings(Game game){

        // game , object list and batch
        this.game = game;
        objects = new ArrayList<GameObject>();
        batch = new SpriteBatch();

        // timers
        removeMarkedObjectsTimer = new Timer(); // object removal
        spawmObstacleTimer = new Timer(); // obstacle spawning
        checkCollisionsTimer = new Timer(); // collision check

        //mathdataArray
        mathData = new String[2];
        combo = 0;

        // scoreStuff
        Preferences prefs = Gdx.app.getPreferences("KiwiDashPreferences");
        getPrefs();
        listener = new TextInput(this);

        // asset manager
        assetManager = new AssetManager(this);
        initializeAllAssets();

        levelReader = new LevelReader();
        currentLevel = levelReader.readLevelData("ForestEasy");

        // screens
        gameScreen = new GameScreen(this);
        menuScreen = new MenuScreen(this);
        tutorialScreen = new TutorialScreen(this);
        highscoreScreen = new HighscoreScreen(this);
        setScreenMenu();

        collisionTask = new ObjectCollisionCheckerTask(this);
        objectRemovalTask = new RemoveMarkedObjectsTask(this);
        objectSpawnerTask = new TestObjectSpawnerTask(this);
        speed = 100f;

        // prevent unwanted closing
        Gdx.input.setCatchBackKey(true);
        inputDelayManager = new InputDelayManager(this);
    }

    /**
     * Contains the Player input for the tilting controls
     * with the accelerometer
     * and the UI buttons.
     *
     */
    public void getPlayerInput(){
        /**
         * The X-axis value from the accelerometer.
         */
        float accel = Gdx.input.getAccelerometerX();

        player.moveMobile(accel);

        // desktop input
        if (Gdx.input.isKeyPressed(Input.Keys.DPAD_LEFT) && !player.getRollManager().isRolling()) {
            player.moveLeft();
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DPAD_RIGHT) && !player.getRollManager().isRolling()) {
            player.moveRight();
        }

        // Vector for the touch position.
        if(Gdx.input.justTouched() && !player.getRollManager().isRolling()){
            touchPosition = new Vector3();
            touchPosition.set(Gdx.input.getX(), Gdx.input.getY(), 0);

            uiCamera.unproject(touchPosition);

            // Dividing the screen into quarters.
            if (touchPosition.x < getWorldWidth() / 4
                    && touchPosition.y <= worldHeight/5){

                setUiData(1);
                dataCheck();
            }
            else if (touchPosition.x > getWorldWidth() / 4
                    && touchPosition.x < getWorldWidth() / 2
                    && touchPosition.y <= worldHeight/5){

                setUiData(2);
                dataCheck();
            }
            else if (touchPosition.x > getWorldWidth() / 2
                    && touchPosition.x < getWorldWidth() - (getWorldWidth() / 4)
                    && touchPosition.y <= worldHeight/5){

                setUiData(3);
                dataCheck();
            }
            else if (touchPosition.x > getWorldWidth() - (getWorldWidth() / 4)
                    && touchPosition.y <= worldHeight/5) {

                setUiData(4);
                dataCheck();
            }

            System.out.println("combo: "+combo);
        }

    }

    /**
     * Contains score and combo calculations for the game.
     * <p>
     * Compares player input with the correct input and manipulates the players score
     * and combo with different game conditions.
     *
     */
    private void dataCheck(){

        gameScreen.setMenuButtonPressed((short)uiData);

        if(uiData == Integer.parseInt(mathData[0])  ){

            int tmp = Integer.valueOf((mathData[0]));
            short tmp2 = (short)tmp;
            gameScreen.explosion((tmp2));

            // maximum combo
            if(combo < 10){
                combo++;

            }
            else{
                int score = 0;

                if(gameScreen.getScoreManager().getScore() >= 100000){
                    score = 400;
                }
                else if(gameScreen.getScoreManager().getScore() >= 50000){
                    score = 300;
                }
                else if(gameScreen.getScoreManager().getScore() >= 25000){
                    score = 200;
                }
                else{
                    score = 100;
                }
                if(getCurrentLevel().getName().contains("mushroom")){
                    score = score * 3;
                }
                gameScreen.getScoreManager().setScore(gameScreen.getScoreManager().getScore() + score);
            }
        }
        else{
            // if no combo, start rolling, otherwise reset combo
                combo = 0;
                player.getRollManager().startRolling();
        }
        emptyMath();
        mathData = createMath();
    }
    // input delay
    private InputDelayManager inputDelayManager;

    /**
     * Creates the mathematical equations for the game.
     * <p>
     * Randomizes the operators and an equation from the four basic mathematical operations,
     * creates a String from the operators and puts the String and the equation in a String array.
     *
     * @return Returns the randomized equation and the correct operation in an array.
     *
     */
    public String[] createMath(){

        String[] mathArray = new String[2];
        /**
         * Randomize the operation for the equation.
         */
        int whatOper = (MathUtils.random(0, 3) + 1);

        /**
         * If the player has over 50000 score, there's a 1/4 chance for negative values to appear in the calculations.
         */
        int lessminusCalcs = (MathUtils.random(0, 3) + 1);

        /**
         * The three operators for the equation as well as the string to combine them.
         */
        int num1;
        int num2;
        int answer;
        String oper="";

        /**
         * Randomize and build the calculation.
         */
        switch (whatOper) {

            // Addition
            case 1:
                if((gameScreen.getScoreManager().getScore() >= 50000)){

                    if (lessminusCalcs == 1) {
                        do {
                            num1 = MathUtils.random(-99, 99);
                            num2 = MathUtils.random(-99, 99);
                        } while ((num1 == -2 && num2 == -2) || (num1 == 2 && num2 == 2) || (num1 == 1 || num2 == 1) ||
                                (num1 == -1 || num2 == -1) || (num1 == 0 || num2 == 0));

                        answer = num1 + num2;

                        if (num2 < 0) {
                            oper = Integer.toString(num1) + " " + "?" + " " + "(" +
                                    Integer.toString(num2) + ")" + " " + "=" + " " + Integer.toString(answer);
                        } else {
                            oper = Integer.toString(num1) + " " + "?" + " " +
                                    Integer.toString(num2) + " " + "=" + " " + Integer.toString(answer);
                        }
                    } else {
                        do {
                            num1 = MathUtils.random(2, 99);
                            num2 = MathUtils.random(2, 99);
                        } while (num1 == 2 && num2 == 2);
                        answer = num1 + num2;
                        oper = Integer.toString(num1) + " " + "?" + " " +
                                Integer.toString(num2) + " " + "=" + " " + Integer.toString(answer);

                    }

                }
                else{
                    num1 = MathUtils.random(2, 99);
                    num2 = MathUtils.random(2, 99);
                } while (num1 == 2 && num2 == 2);
                answer = num1 + num2;
                oper = Integer.toString(num1) + " " + "?" + " " +
                        Integer.toString(num2) + " " + "=" + " " + Integer.toString(answer);

                break;

            //  Subtraction
            case 2:
                if((gameScreen.getScoreManager().getScore() >= 50000)){

                    if (lessminusCalcs == 1) {
                        do {
                            num1 = MathUtils.random(-99, 99);
                            num2 = MathUtils.random(-99, 99);
                        }
                        while ((num1 == -2 && num2 == -2) || (num1 == 2 && num2 == 2) || (num1 == 1 || num2 == 1) ||
                                (num1 == -1 || num2 == -1) || (num1 == 0 || num2 == 0));

                        answer = num1 + num2;

                        if (num1 < 0) {
                            oper = Integer.toString(answer) + " " + "?" + " " + "(" +
                                    Integer.toString(num1) + ")" + " " + "=" + " " + Integer.toString(num2);
                        } else {
                            oper = Integer.toString(answer) + " " + "?" + " " +
                                    Integer.toString(num1) + " " + "=" + " " + Integer.toString(num2);
                        }
                    }

                    else {
                        do {
                            num1 = MathUtils.random(2, 99);
                            num2 = MathUtils.random(2, 99);
                        } while(num1 == 2 && num2 == 2);
                        answer = num1 + num2;
                        oper = Integer.toString(answer) + " " + "?" + " " +
                                Integer.toString(num1) + " " + "=" + " " + Integer.toString(num2);
                    }
                }
                else{
                    do {
                        num1 = MathUtils.random(2, 99);
                        num2 = MathUtils.random(2, 99);
                    } while(num1 == 2 && num2 == 2);
                    answer = num1 + num2;
                    oper = Integer.toString(answer) + " " + "?" + " " +
                            Integer.toString(num1) + " " + "=" + " " + Integer.toString(num2);
                }

                break;

            // Multiplication
            case 3:

                if((gameScreen.getScoreManager().getScore() >= 50000)){

                    if (lessminusCalcs == 1) {
                        do {
                            num1 = MathUtils.random(-10, 10);
                            num2 = MathUtils.random(-10, 10);
                        }
                        while ((num1 == -2 && num2 == -2) || (num1 == 2 && num2 == 2) || (num1 == 1 || num2 == 1) ||
                                (num1 == -1 || num2 == -1) || (num1 == 0 || num2 == 0));

                        answer = num1 * num2;

                        if (num2 < 0) {
                            oper = Integer.toString(num1) + " " + "?" + " " + "(" +
                                    Integer.toString(num2) + ")" + " " + "=" + " " + Integer.toString(answer);
                        } else {
                            oper = Integer.toString(num1) + " " + "?" + " " +
                                    Integer.toString(num2) + " " + "=" + " " + Integer.toString(answer);
                        }
                    }

                    else{
                        do {
                            num1 = MathUtils.random(2, 10);
                            num2 = MathUtils.random(2, 10);
                        } while(num1 == 2 && num2 == 2);
                        answer = num1 * num2;
                        oper = Integer.toString(num1) + " " + "?" + " " +
                                Integer.toString(num2) + " " + "=" + " " +  Integer.toString(answer);

                    }
                }
                else{
                    do {
                        num1 = MathUtils.random(2, 10);
                        num2 = MathUtils.random(2, 10);
                    } while(num1 == 2 && num2 == 2);
                    answer = num1 * num2;
                    oper = Integer.toString(num1) + " " + "?" + " " +
                            Integer.toString(num2) + " " + "=" + " " +  Integer.toString(answer);
                }

                break;

            // Division
            case 4:
                if((gameScreen.getScoreManager().getScore() >= 50000)){
                    if (lessminusCalcs == 1) {
                        do {
                            num1 = MathUtils.random(-10, 10);
                            num2 = MathUtils.random(-10, 10);
                        }
                        while ((num1 == -2 && num2 == -2) || (num1 == 2 && num2 == 2) || (num1 == 1 || num2 == 1) ||
                                (num1 == -1 || num2 == -1) || (num1 == 0 || num2 == 0));

                        answer = num1 * num2;

                        if (num1 < 0) {
                            oper = Integer.toString(answer) + " " + "?" + " " + "(" +
                                    Integer.toString(num1) + ")" + " " + "=" + " " + Integer.toString(num2);
                        } else {
                            oper = Integer.toString(answer) + " " + "?" + " " +
                                    Integer.toString(num1) + " " + "=" + " " + Integer.toString(num2);
                        }
                    }

                    else{
                        do {
                            num1 = MathUtils.random(2, 10);
                            num2 = MathUtils.random(2, 10);
                        } while(num1 == 2 && num2 == 2);
                        answer = num1 * num2;
                        oper = Integer.toString(answer) + " " + "?" + " " +
                                Integer.toString(num1) + " " + "=" + " " +  Integer.toString(num2);

                    }

                }
                else{
                    do {
                        num1 = MathUtils.random(2, 10);
                        num2 = MathUtils.random(2, 10);
                    } while(num1 == 2 && num2 == 2);
                    answer = num1 * num2;
                    oper = Integer.toString(answer) + " " + "?" + " " +
                            Integer.toString(num1) + " " + "=" + " " +  Integer.toString(num2);

                }

                break;
        }
        // Insert the correct operation and the equation to an array.
        mathArray[0]=Integer.toString(whatOper);
        mathArray[1]=oper;
        return mathArray;

    }

    /**
     * Replaces the current calculation and input with the default values.
     */
    public void emptyMath(){
        mathData[0]="0";
        mathData[1]="";
        setUiData(0);
    }

    /**
     * Creates an animation from a spritesheet.
     *
     * @param spriteSheet The spritesheet for the wanted animation.
     * @param cols The amount of columns in the spritesheet.
     * @param rows The amount of rows in the spritesheet.
     * @param offset The amount of blank offset slots in the spritesheet.
     * @param speed The speed the animation is played.
     * @return Returns the created Animation.
     */
    public Animation createAnimation(Texture spriteSheet,short cols, short rows, short offset, float speed){

        TextureRegion[][] tmp1 = TextureRegion.split(spriteSheet, spriteSheet.getWidth()/cols, spriteSheet.getHeight()/rows);
        TextureRegion [] frames1 = transformTo1D(tmp1, cols, rows, offset);
        Animation animation = new Animation(speed,frames1);

        return animation;
    }

    /**
     * Randomizes a value from the given range.
     *
     * @param min The desired minimum value.
     * @param max The desired maximum value.
     * @return Returns the randomized value.
     */
    public int randomWithRange(int min, int max) {
        int range = (max - min) + 1;
        return (int)(Math.random() * range) + min;
    }

    /**
     * Transforms a 2D spritesheet into 1D
     *
     * @param temp The 2D array that is converted.
     * @param FRAME_COLS The amount of columns in the spritesheet.
     * @param FRAME_ROWS The amount of rows in the spritesheet.
     * @param offset The amount of blank offset slots in the spritesheet.
     * @return Returns the converted sheet in 1D.
     */
    public TextureRegion[] transformTo1D(TextureRegion[][] temp, int FRAME_COLS, int FRAME_ROWS, int offset) {

        TextureRegion[] frames = new TextureRegion[(FRAME_COLS * FRAME_ROWS) - offset];
        int index = 0;

        for (int n = 0; n < FRAME_ROWS; n++) {

            if (n == FRAME_ROWS - 1) {

                for (int m = 0; m < FRAME_COLS - offset; m++) {
                    frames[index++] = temp[n][m];
                }
            } else {
                for (int m = 0; m < FRAME_COLS; m++) {
                    frames[index++] = temp[n][m];
                }
            }

        }

        return frames;
    }

    // timers
    public void resetTimers(){
        System.out.println("Clearing timers.");
        spawmObstacleTimer.clear();
        checkCollisionsTimer.clear();
        removeMarkedObjectsTimer.clear();

    }
    public void startTimers(){
        System.out.println("Starting timers.");
        // start timers
        spawmObstacleTimer.scheduleTask(objectSpawnerTask,0, 0.1f);
        checkCollisionsTimer.scheduleTask(collisionTask,0, 0.1f);
        removeMarkedObjectsTimer.scheduleTask(objectRemovalTask,0, 10);
    }

    // asset managing
    public void disposeAllAssets(){
        assetManager.dispose();
    }
    private void initializeAllAssets(){
        assetManager.initializeTextures();
        assetManager.initializeAllSounds();
    }
    public void updateAllAssets(){
        assetManager.initializeTextures();
        assetManager.initializeMusic();
        assetManager.initializeAllSounds();
    }
    public AssetManager getAssetManager(){
        return assetManager;
    }

    /**
     * Sets the game level to the level give to the method.
     *
     * @param filename The filename for the level to be set.
     */
    public void setCurrentLevel(String filename){
        System.out.println("Setting current level to: "+filename);

        String lastLevelName = currentLevel.getName();

        currentLevel = levelReader.readLevelData(filename);
        gameScreen.matchBackgroundLevel();
        objectSpawnerTask.matchTexturesLevel();
        objectSpawnerTask.updateData();
        if(lastLevelName.contains("mushroom") || currentLevel.getName().contains("mushroom")){
            assetManager.updateObjectTexturesLevelChange();
        }
        gameScreen.updateMusic();
        if(currentLevel.getName().contains("desert")){
            gameScreen.setDududu(true);
        }
        else{
            gameScreen.setDududu(false);
        }
    }
    public LevelData getCurrentLevel(){
        return currentLevel;
    }

    /**
     * Sets a highscore with the player name & score to the current highscore list.
     *
     * @param currentScore The score that made it to highscores.
     */
    public void setHighscore(int currentScore){

        String[] names = new String[10];
        int[] values = new int[10];
        int scorePosition = 10;

        String highScoreName = "HighscoreName";
        String highScoreValue = "HighscoreValue";

        names = getHighscoreNames();
        values = getHighscoreValues();

        // set new score
        for(int n = 0; n < values.length; n++){
            if(currentScore >= values[n]){
                scorePosition --;
            }
        }

        // set old scores
        for(int n = 0; n < 10; n++){
            if(values[n] <= currentScore){
                prefs.putInteger(highScoreValue + (n +1) ,values[n]);
                prefs.putString(highScoreName + (n +1) ,names[n]);

            }
        }
        //set new score
        highScoreName += scorePosition;
        highScoreValue += scorePosition;

        // highscore name
        prefs.putString(highScoreName, listener.getUsername() );
        // highscore score
        prefs.putInteger(highScoreValue, currentScore);

        System.out.println("Setting highscore:"+ highScoreName);
        System.out.println("Setting highscore:"+ highScoreValue);

        prefs.flush();
    }
    public void setMute(boolean muted){
        prefs.putBoolean("Muted", muted);
        prefs.flush();
        System.out.println("Sound muted : "+muted);
    }
    public void setTutorialDone(boolean done){
        prefs.putBoolean("TutorialDone", done);
        prefs.flush();
    }
    public boolean isMuted(){
        if(prefs != null){
            if(prefs.contains("Muted")){
                return prefs.getBoolean("Muted");
            }
            else{
                return false;
            }
        }
        else{
            System.out.println("Preferences null");
            return false;
        }
    }
    public boolean isTutorialDone(){
        if(prefs != null){
            if(prefs.contains("TutorialDone")){
                return prefs.getBoolean("TutorialDone");
            }
            else{
                return false;
            }
        }
        else{
            System.out.println("Preferences null");
            return false;
        }

    }
    public String[] getHighscoreNames(){
        // get names for existing entries
        String[] names = new String[10];


        for(int n = 0; n < names.length; n++){
            if(prefs != null){
                names[n] = prefs.getString("HighscoreName" +n,"-no name set-");
            }
            else{
                names[n] = "not initialized yet";
            }
        }
        return names;
    }
    public int[] getHighscoreValues(){
        // get values for existing entries
        int[] values = new int[10];

        for(int n = 0; n < values.length; n++) {
            if(prefs != null){
                values[n] = prefs.getInteger("HighscoreValue" + n, 0);
            }
            else{
                values[n] = 0;
            }

        }
        return values;
    }
    protected Preferences getPrefs() {
        if(prefs==null){
            prefs = Gdx.app.getPreferences("KiwiDashPreferences");
        }
        return prefs;
    }

    /**
     * Resets the Highscores list.
     */
    public void resetScores(){
        for(int n = 0; n < 10; n++) {
            prefs.putInteger("HighscoreValue" +n, 0);
            prefs.putString("HighscoreName" + n, "-no score set-");
            prefs.flush();
        }
    }

    // setters
    public void setMathData(String[] mathData) {
        this.mathData = mathData;
    }
    public void setUiData(int uiData) {
        this.uiData = uiData;
    }
    public void setUiCamera(OrthographicCamera camera){
        uiCamera = camera;
    }
    public void setMovementSpeed(float speed){
        this.speed = speed;
    }
    public void setCombo(int combo){
        this.combo = combo;
    }

    // screen setters
    public void setScreenGame(){

        // create player
        player = new Player(this);
        objects.add(player);

        // starts timers
        objectSpawnerTask = new TestObjectSpawnerTask(this); // to prevent level reading bug on restart
        spawmObstacleTimer.scheduleTask(objectSpawnerTask, 5, 0.1f);
        checkCollisionsTimer.scheduleTask(collisionTask, 1, 0.1f);
        removeMarkedObjectsTimer.scheduleTask(objectRemovalTask, 1, 10);
        gameScreen.getScoreManager().startTimer();
        gameScreen.getPowerupManager().startTimer();

        // player animations
        player.animations();

        // first calculation
        combo = 0;
        mathData = createMath();

        // update textures
        gameScreen.updateTextures();
        gameScreen.updateParticles();
        game.setScreen(gameScreen);

        // reset background
        gameScreen.resetBackground();

        assetManager.initializeMusic();
        gameScreen.playMusic();
        gameScreen.updateTextures();

        // starting level
        setCurrentLevel("ForestEasy");
    }
    public void setScreenMenu(){

        //
        resetGameData();

        // update textures
        menuScreen.updateTextures();

        gameScreen.stopMusic();

        // set screen
        game.setScreen(menuScreen);

    }
    public void setScreenTutorial(){
        game.setScreen(tutorialScreen);
    }
    public void setScreenHighscore(){
        gameScreen.stopMusic();
        game.setScreen(highscoreScreen);
    }
    public void resetGameData(){

        gameScreen.setPausedState(false);
        gameScreen.getScoreManager().setScore(0);
        gameScreen.getScoreManager().getScoreTimer().clear();
        gameScreen.getPowerupManager().getSpawnPowerupTimer().clear();

        // reset timers
        resetTimers();

        // clear objects
        objects.clear();

        // clear current math
        emptyMath();
    }

    // getters
    public String[] getMathData() {
        return mathData;
    }
    public int getUiData(){
        return uiData;
    }
    public Player getPlayer(){
        return player;
    }
    public ArrayList<GameObject> getObjects() {
        return objects;
    }
    public SpriteBatch getBatch() {
        return batch;
    }
    public float getWorldWidth() {
        return worldWidth;
    }
    public float getWorldHeight() {
        return worldHeight;
    }
    public float getSpeed(){
        return speed;
    }
    public int getCombo() {
        return combo;
    }
    public GameScreen getGameScreen(){
        return gameScreen;
    }
    public Game getGame(){
        return game;
    }
    public TextInput getListener(){
        listener = new TextInput(this);
        return listener;
    }
    public InputDelayManager getInputDelayManager() {
        return inputDelayManager;
    }
}
