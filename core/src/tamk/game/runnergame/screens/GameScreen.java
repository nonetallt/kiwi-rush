package tamk.game.runnergame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Timer;
import tamk.game.runnergame.Settings;
import tamk.game.runnergame.objects.GameObject;
import tamk.game.runnergame.objects.ObstacleTest;
import tamk.game.runnergame.objects.Player;
import tamk.game.runnergame.objects.Powerup;
import tamk.game.runnergame.timerTasks.LimpTimerTask;
import tamk.game.runnergame.timerTasks.PowerupSpawnerTask;
import tamk.game.runnergame.timerTasks.RollTimerTask;
import tamk.game.runnergame.timerTasks.ScoreTimerTask;
import tamk.game.runnergame.utilities.PowerupManager;
import tamk.game.runnergame.utilities.ScoreManager;


import java.util.ArrayList;


public class GameScreen implements Screen{

    // debug
    private final String TAG = "GameScreen";

    // textures
    private Texture backgroundTexture;
    private Texture backgroundTopTexture;
    private Texture forestDesert;
    private Texture desertForest;

    private Texture calcTexture;
    private Texture plusTexture;
    private Texture minusTexture;
    private Texture multiplyTexture;
    private Texture divideTexture;
    private Texture playerHealthTexture;
    private Texture statusbar;

    private boolean plusPressed = false;
    private boolean minusPressed = false;
    private boolean multiplyPressed = false;
    private boolean dividePressed = false;

    // back button
    private boolean backPressed = false;
    private Texture backButton;
    private Texture backButtonPressed;

    // mute button
    private Texture soundOn;
    private Texture soundOnPressed;
    private Texture soundOff;
    private Texture soundOffPressed;
    private boolean buttonHoldSound = false;

    // camera & batch & stuff
    private OrthographicCamera staticCamera;
    private OrthographicCamera uiCamera;
    private SpriteBatch batch;

    // settings & utilities
    private Settings settings;
    private boolean paused = false;

    // backgroundstuff
    private float lastY = 0;
    private  float secondLastY = 800f;
    private float lastY2 = 0;
    private  float secondLastY2 = 800f;
    private float lastTransition = 800f;
    private boolean transition = false;
    private  boolean drawTransition = false;

    // timer
    private float timeElapsed;
    private float limpTime;
    private float scoreTime;
    private float powerupTime;
    private boolean gatherTime;
    private boolean gatherTimeLimp;
    private ScoreManager scoreManager;
    private PowerupManager powerupManager;

    // music
    private Music currentMusic;
    private float musicState;
    private String musicName;

    // font + particle
    private BitmapFont font;
    private ParticleEffect explosionPlus;
    private ParticleEffect explosionMinus;
    private ParticleEffect explosionMultiply;
    private ParticleEffect explosionDivide;
    private ParticleEffect sandstorm;
    private boolean dududu = false;


    // constructor
    public GameScreen(Settings settings){

        this.settings = settings;

        gatherTime = false;
        gatherTimeLimp = false;
        scoreManager = new ScoreManager(settings);
        powerupManager = new PowerupManager(settings);

        // rendering
        batch = settings.getBatch();

        staticCamera = new OrthographicCamera();
        staticCamera.setToOrtho(false, settings.getWorldWidth(), settings.getWorldHeight());
        uiCamera = new OrthographicCamera();
        uiCamera.setToOrtho(false, settings.getWorldWidth(), settings.getWorldHeight());

        settings.setUiCamera(uiCamera);
    }

    // screen methods
    @Override
    public void render(float delta){

        // static camera for objects
        batch.setProjectionMatrix(staticCamera.combined);

        // buffer bit
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            batch.begin();
            gameRender();
            batch.end();

        // static camera rendering for menus
        batch.setProjectionMatrix(uiCamera.combined);

            batch.begin();
            drawSandstorm(delta);
            uiRender(delta);
            batch.end();

        fingerLifted();

    }
    @Override
    public void show() {
        Gdx.app.log(TAG, "show()");

    }
    @Override
    public void resize(int width, int height) {
        Gdx.app.log(TAG, "resize width = " + width + ", height = " + height);

    }
    @Override
    public void pause() {
        Gdx.app.log(TAG, "pause()");
        paused = true;
        musicState = currentMusic.getPosition();
        currentMusic.stop();
        settings.resetTimers();
        settings.getPlayer().getRollManager().getRollTimer().clear();
        settings.getPlayer().getLimpManager().getLimpTimer().clear();
        scoreManager.getScoreTimer().clear();
        powerupManager.getSpawnPowerupTimer().clear();
        disposeAssets();
    }
    @Override
    public void resume() {
        settings.updateAllAssets();
        updateTextures();
        settings.getPlayer().animations();
        updateParticles();

        // music
        if(!settings.isMuted()){
            currentMusic = settings.getAssetManager().getMusic();
            currentMusic.setLooping(true);
            currentMusic.play();
            currentMusic.setPosition(musicState);
        }

        Gdx.app.log(TAG, "resume()");
    }
    @Override
    public void hide() {
        Gdx.app.log(TAG, "hide()");

    }
    @Override
    public void dispose() {
        Gdx.app.log(TAG, "dispose()");
        disposeParticles();
    }

    /**
     * Draws the background, moves the objects, gets player input.
     */
    private void gameRender(){

        // back button on android
        androidBackInput();

        if(!paused){

            // timers
            if(gatherTime){
                timeElapsed += Gdx.graphics.getDeltaTime();
            }
            if(gatherTimeLimp){
                limpTime += Gdx.graphics.getDeltaTime();
            }
            scoreTime += Gdx.graphics.getDeltaTime();
            powerupTime += Gdx.graphics.getDeltaTime();

            // scale movement speed
            settings.setMovementSpeed(100f + settings.getCombo() * 12.5f);
            settings.getPlayer().setAnimationSpeed((float)1 / (20 + (settings.getCombo() * 1.5f) ) );

            // input, if player is dead, unable to move
            if(!settings.getPlayer().isDead()){
                settings.getPlayerInput();
            }

            // obstacle movement
            ArrayList<GameObject> objects = settings.getObjects();
            for(GameObject object : objects){
                if(object instanceof ObstacleTest || object instanceof Powerup){
                    object.moveDown();
                }
            }

            // gravity for player
            settings.getPlayer().getJumpManager().gravityCheck();

            // draw background
            drawBackground(backgroundTexture);
            transition();

            // draw objects in game
            objectLayersRender();

            // draw upper layer of background
            //drawBackground2nd(backgroundTopTexture);

        }
        else{
            // paused stuff

            // input
            if(Gdx.input.justTouched()) {
                Vector3 touchPosition = new Vector3();
                touchPosition.set(Gdx.input.getX(), Gdx.input.getY(), 0);
                staticCamera.unproject(touchPosition);

                float width = settings.getWorldWidth();
                float height = settings.getWorldHeight();

                if( (touchPosition.x >= width * 0.05f && touchPosition.x <= width * 0.05f + 85)
                        &&(touchPosition.y >= height * 0.025f && touchPosition.y <= height * 0.025f + 70) ){
                    backPressed = true;
                }
                else if((touchPosition.x >= width - width * 0.05f -85 && touchPosition.x <= width - width * 0.05f)
                        &&     (touchPosition.y >= height * 0.025f && touchPosition.y <= height * 0.025f + 70) ){
                    if(settings.isMuted()){
                        settings.setMute(false);
                        buttonHoldSound = true;
                        playMusic();
                    }
                    else{
                        settings.setMute(true);
                        buttonHoldSound = true;
                        currentMusic.stop();
                    }
                }
                else{
                    unpause();
                }
            }
            else if(!Gdx.input.isTouched()){
                if(backPressed){
                    backPressed = false;
                    settings.setScreenMenu();
                }
                if(buttonHoldSound){
                    buttonHoldSound = false;
                }
            }

            // draw background
            batch.draw(backgroundTexture, 0, lastY, 480, 3200);

            // draw transition background
            if(drawTransition){
                if(settings.getCurrentLevel().getName().contains("desert")){
                    batch.draw(forestDesert,0 ,lastTransition,480, 1600);
                }
                else if(settings.getCurrentLevel().getName().contains("forest")){
                    batch.draw(desertForest,0 ,lastTransition,480, 1600);
                }
            }

            // draw objects in game
            objectLayersRender();

            // draw upper layer of background
            /**
            if(settings.getCurrentLevel().getName().contains("forest")){
                batch.draw(backgroundTopTexture, 0, lastY2, 480, 3200);
            }
             */

            // back button
            drawBackButton();

            // mute button
            drawMuteButton();

            // draw pause text
            if(font != null){
                font.draw(batch, "Game paused", (settings.getWorldWidth()  / 2) - (font.getBounds("Game paused").width / 2), settings.getWorldHeight() / 2 + font.getBounds("Game paused").height);
                font.draw(batch, "Tap screen to resume", (settings.getWorldWidth()  / 2) - (font.getBounds("Tap screen to resume").width / 2), settings.getWorldHeight() / 2 + font.getBounds("Tap screen to resume").height - 50);
            }
        }


    }

    /**
     * Draws the game objects based on layers.
     */
    private void objectLayersRender(){
        ArrayList<GameObject> objectList =  settings.getObjects(); // all objects in game
        ArrayList<GameObject> objectListLowest =  new ArrayList<GameObject>(); // objects drawn first, "under background"
        ArrayList<GameObject> objectListLower =  new ArrayList<GameObject>(); // objects drawn first, "background"
        ArrayList<GameObject> objectListUpper =  new ArrayList<GameObject>(); // objects drawn on top of first layer

        for(GameObject object: objectList ){
            if(object instanceof Powerup){
                objectListLowest.add(object);
            }
            else if(object.getY() < settings.getPlayer().getY() && !(object instanceof Player)){
                objectListUpper.add(object);
            }
            else if(!(object instanceof Player)){
                objectListLower.add(object);
            }
        }

        // draw objects from lowest layer
        for(GameObject object: objectListLowest ){
            object.draw(batch);
        }
        // draw objects from lower layer
        for(GameObject object: objectListLower ){
            object.draw(batch);
        }
        // if on ground, draw player
        if(!settings.getPlayer().getJumpManager().isOnAir()){
            settings.getPlayer().drawPlayer(); // draw player
        }
        // draw objects from upper layer
        for(GameObject object: objectListUpper ){
            object.draw(batch);
        }
        // if not on ground, draw player on top layer
        if(settings.getPlayer().getJumpManager().isOnAir()){
            settings.getPlayer().drawPlayer(); // draw player
        }

    }

    /**
     * Draws the UI.
     * @param delta
     */
    private void uiRender(float delta){

        // draw statusbar
        if(statusbar != null){
            batch.draw(statusbar,0,settings.getWorldHeight() - statusbar.getHeight());
        }

        // draw health
        float display = playerHealthTexture.getWidth();

        for(int n = 0; n < settings.getPlayer().getHealth(); n++){
            batch.draw(playerHealthTexture, display*n, settings.getWorldHeight() - (display * 1.15f) );
        }

        // draw combo + score
        if(font != null){
            font.setScale(0.7f);
            String scoreText =  "" + scoreManager.getScore();
            String comboText = "x " + settings.getCombo();

            float xPos = settings.getWorldWidth() - font.getBounds(scoreText).width;
            float yPos = settings.getWorldHeight();

            font.draw(batch, scoreText, xPos, yPos );
            font.draw(batch, comboText, settings.getWorldWidth() - font.getBounds(comboText).width, yPos -40);
            font.setScale(0.8f);

        }

        if(!paused){

            drawExplosions(delta);

            // draw calc
            String[] mathArray = settings.getMathData();
            if (!(mathArray[1] == null) && font != null) {

                float xPos = (settings.getWorldWidth()  / 2) - (font.getBounds(mathArray[1]).width / 2);
                float yPos = settings.getWorldHeight() - settings.getWorldHeight() / 8;

                font.draw(batch, mathArray[1], xPos, yPos );
            }
            // draw bottom screen buttons
            if (plusPressed){
                batch.draw(plusTexture,0,0);
            }
            else if (minusPressed){
                batch.draw(minusTexture,0,0);
            }
            else if (multiplyPressed){
                batch.draw(multiplyTexture,0,0);
            }
            else if (dividePressed){
                batch.draw(divideTexture,0,0);
            }
            else{
                batch.draw(calcTexture,0,0);
            }


        }
    }

    /**
     * Gets the UI- and game textures.
     */
    public void updateTextures(){
        matchBackgroundLevel();
        calcTexture = settings.getAssetManager().getTexture("buttons.jpg");
        plusTexture = settings.getAssetManager().getTexture("buttonsp.jpg");
        minusTexture = settings.getAssetManager().getTexture("buttonsm.jpg");
        multiplyTexture = settings.getAssetManager().getTexture("buttonsx.jpg");
        divideTexture = settings.getAssetManager().getTexture("buttonsd.jpg");
        playerHealthTexture = settings.getAssetManager().getTexture("healthTexture.png");
        backButton = settings.getAssetManager().getTexture("backButton.png");
        backButtonPressed = settings.getAssetManager().getTexture("backButtonPressed.png");
        soundOn = settings.getAssetManager().getTexture("soundOn.png");
        soundOnPressed = settings.getAssetManager().getTexture("soundOnPressed.png");
        soundOff = settings.getAssetManager().getTexture("soundOff.png");
        soundOffPressed = settings.getAssetManager().getTexture("soundOffPressed.png");
        statusbar = settings.getAssetManager().getTexture("statusbar.jpg");
        forestDesert = settings.getAssetManager().getTexture("forest-desert.jpg");
        desertForest = settings.getAssetManager().getTexture("desert-forest.jpg");
        settings.getAssetManager().updateObjectTextures();
    }

    /**
     * Draws the mute button.
     */
    private void drawMuteButton(){
        if(settings.isMuted()){
            if(!buttonHoldSound){
                batch.draw(soundOff, settings.getWorldWidth() - settings.getWorldWidth() * 0.05f -85, settings.getWorldHeight() * 0.025f);
            }
            else{
                batch.draw(soundOffPressed, settings.getWorldWidth() - settings.getWorldWidth() * 0.05f -85, settings.getWorldHeight() * 0.025f);
            }
        }
        else{
            if(!buttonHoldSound){
                batch.draw(soundOn, settings.getWorldWidth() - settings.getWorldWidth() * 0.05f -85, settings.getWorldHeight() * 0.025f);
            }
            else{
                batch.draw(soundOnPressed, settings.getWorldWidth() - settings.getWorldWidth() * 0.05f -85, settings.getWorldHeight() * 0.025f);
            }
        }
    }
    private void drawBackButton(){
        if(backPressed){
            batch.draw(backButtonPressed, settings.getWorldWidth() * 0.05f, settings.getWorldHeight() * 0.025f);
        }
        else{
            batch.draw(backButton, settings.getWorldWidth() * 0.05f, settings.getWorldHeight() * 0.025f);
        }
    }

    /**
     * Draws the background.
     *
     * @param texture The texture for the current level's background.
     */
    private void drawBackground(Texture texture){

        float coordinateY = lastY - (settings.getSpeed() * Gdx.graphics.getDeltaTime() );
        float secondY = secondLastY - (settings.getSpeed() * Gdx.graphics.getDeltaTime() );

        // if 2 textures are needed
        if(coordinateY <= -2400f){

            batch.draw(texture, 0, coordinateY, 480, 3200);
            // stores last coordinate
            lastY = lastY - (settings.getSpeed() * Gdx.graphics.getDeltaTime() );

            if(coordinateY <= -3200f){
                System.out.println("Resetting");
                batch.draw(texture, 0, secondY, 480, 3200);
                lastY = secondY;
                secondLastY = settings.getWorldHeight();
            }
            else{
                batch.draw(texture, 0, secondY, 480, 3200);
                // stores last coordinate
                secondLastY = secondLastY - (settings.getSpeed() * Gdx.graphics.getDeltaTime() );
            }
        }
        else{
            batch.draw(texture, 0, coordinateY, 480, 3200);
            // stores last coordinate
            lastY = lastY - (settings.getSpeed() * Gdx.graphics.getDeltaTime() );
        }
    }
    private void drawBackground2nd(Texture texture){

        if((settings.getCurrentLevel().getName().contains("forest"))){
            float coordinateY = lastY2 - (settings.getSpeed() * Gdx.graphics.getDeltaTime() );
            float secondY = secondLastY2 - (settings.getSpeed() * Gdx.graphics.getDeltaTime() );

            // if 2 textures are needed

            if(coordinateY <= -2400f){
                batch.draw(texture, 0, coordinateY, 480, 3200);
                // stores last coordinate
                lastY2 = lastY2 - (settings.getSpeed() * Gdx.graphics.getDeltaTime() );

                if(coordinateY <= -3200f){
                    System.out.println("Resetting");
                    batch.draw(texture, 0, secondY, 480, 3200);
                    lastY2 = secondY;
                    secondLastY2 = settings.getWorldHeight();
                }
                else{
                    batch.draw(texture, 0, secondY, 480, 3200);
                    // stores last coordinate
                    secondLastY2 = secondLastY2 - (settings.getSpeed() * Gdx.graphics.getDeltaTime() );
                }
            }
            else{
                batch.draw(texture, 0, coordinateY, 480, 3200);
                // stores last coordinate
                lastY2 = lastY2 - (settings.getSpeed() * Gdx.graphics.getDeltaTime() );
            }
        }

    }

    /**
     * Checks which transition texture is needed for the current level.
     */
    private void transition(){
        if(drawTransition){

            if(settings.getCurrentLevel().getName().contains("desert")){
                drawTransition(forestDesert);
            }
            else if(settings.getCurrentLevel().getName().contains("forest")){
                drawTransition(desertForest);
            }
        }
    }

    /**
     * Draws the transition texture.
     * @param texture
     */
    private void drawTransition(Texture texture){
        float coordinateY = lastTransition - (settings.getSpeed() * Gdx.graphics.getDeltaTime() );
        batch.draw(texture,0 ,coordinateY,480, 1600);
        lastTransition = lastTransition - (settings.getSpeed() * Gdx.graphics.getDeltaTime() );

        if(lastTransition < -800){
            transition = false;
            matchBackgroundLevel();
        }
        if(lastTransition < -1600){
            drawTransition = false;
        }
    }
    public void resetBackground(){
        lastY = 0;
        lastY2 = 0;
    }
    public void matchBackgroundLevel(){
        if(!transition){
            if(settings.getCurrentLevel().getName().contains("forest")){
                backgroundTexture = settings.getAssetManager().getTexture("background.jpg");
                backgroundTopTexture = settings.getAssetManager().getTexture("background2top.png");
            }
            else if(settings.getCurrentLevel().getName().contains("mushroom")){
                backgroundTexture = settings.getAssetManager().getTexture("backgroundMushroom.jpg");
            }
            else if(settings.getCurrentLevel().getName().contains("desert")){
                backgroundTexture = settings.getAssetManager().getTexture("backgroundDesert.jpg");
            }
        }
    }

    /**
     * Changes game to active & starts timers
     */
    private void unpause(){
        paused = false;
        settings.startTimers();
        settings.getPlayer().getRollManager().getRollTimer().scheduleTask(new RollTimerTask(settings), 3.0f - timeElapsed);
        settings.getPlayer().getLimpManager().getLimpTimer().scheduleTask(new LimpTimerTask(settings), 5.0f - limpTime);
        scoreManager.getScoreTimer().scheduleTask(new ScoreTimerTask(settings), 0.1f - scoreTime, 0.1f);
        powerupManager.getSpawnPowerupTimer().scheduleTask(new PowerupSpawnerTask(settings), 3f - powerupTime, 3);

    }
    private void disposeAssets(){
        settings.disposeAllAssets();
        disposeParticles();
    }

    // active time
    public void startRunningTime(){
        timeElapsed = 0;
        gatherTime = true;
    }
    public void stopRunningTime(){
        gatherTime = false;
    }
    public void startLimpTime(){
        limpTime = 0;
        gatherTimeLimp = true;
    }
    public void stopLimpTime(){
        gatherTimeLimp = false;
    }
    public void resetScoreTime(){
        scoreTime = 0;
    }
    public void resetPowerupTime(){
        powerupTime = 0;
    }

    // getters
    public ScoreManager getScoreManager(){
        return scoreManager;
    }
    public PowerupManager getPowerupManager(){
        return powerupManager;
    }
    public boolean isDrawingTransition(){
        return drawTransition;
    }

    // setters
    public void setPausedState(boolean paused){
        this.paused = paused;
    }
    public void setMenuButtonPressed(short button){

        if(button == 1){
            plusPressed = true;
        }
        else if(button == 2){
            minusPressed = true;
        }
        else if(button == 3){
            multiplyPressed = true;
        }
        else if(button == 4){
            dividePressed = true;
        }
    }
    public void setDududu(boolean dududu) {
        this.dududu = dududu;
    }
    public void setTransition(boolean transition) {
        this.transition = transition;
        drawTransition = transition;
        lastTransition = 800f;
    }

    // input
    private void androidBackInput(){
        if (Gdx.input.isKeyPressed(Input.Keys.BACK) && !settings.getInputDelayManager().isActive()){

            settings.getInputDelayManager().startDelay();

            System.out.println("Android back button pressed.");
            settings.resetTimers();

            if(paused){
                settings.setScreenMenu();
            }
            else{
                paused = true;
                settings.resetTimers();
                settings.getPlayer().getRollManager().getRollTimer().clear();
                settings.getPlayer().getLimpManager().getLimpTimer().clear();
                scoreManager.getScoreTimer().clear();
                powerupManager.getSpawnPowerupTimer().clear();
            }
        }
    }
    private void fingerLifted() {
        if (!Gdx.input.isTouched()) {
            if(plusPressed){
                plusPressed = false;
            }
            else if(minusPressed){
                minusPressed = false;
            }
            else if(multiplyPressed){
                multiplyPressed = false;
            }
            else if(dividePressed){
                dividePressed = false;
            }
        }
    }

    // music
    public void playMusic(){
        musicState = 0;
        currentMusic = settings.getAssetManager().getMusic();
        currentMusic.setLooping(true);
        if(!settings.isMuted()){
            currentMusic.play();
        }

        musicName = settings.getCurrentLevel().getName();
    }
    public void updateMusic(){

        if(!settings.getCurrentLevel().getName().contains(musicName)){
            settings.getAssetManager().initializeMusic();
            currentMusic.stop();

            musicState = 0;
            currentMusic = settings.getAssetManager().getMusic();
            currentMusic.setLooping(true);
            if(!settings.isMuted()){
                currentMusic.play();
            }
            musicName = settings.getCurrentLevel().getName();
        }
    }
    public void stopMusic(){
        if(currentMusic != null){
            currentMusic.stop();
        }
    }

    // particle
    // also updates font
    public void updateParticles(){
        disposeParticles();

        explosionPlus = new ParticleEffect();
        explosionMinus = new ParticleEffect();
        explosionMultiply = new ParticleEffect();
        explosionDivide = new ParticleEffect();
        sandstorm = new ParticleEffect();

        explosionPlus.load(Gdx.files.internal("effects/explosionPlus.p"),Gdx.files.internal("effects/"));
        explosionMinus.load(Gdx.files.internal("effects/explosionMinus.p"),Gdx.files.internal("effects/"));
        explosionMultiply.load(Gdx.files.internal("effects/explosionMultiply.p"),Gdx.files.internal("effects/"));
        explosionDivide.load(Gdx.files.internal("effects/explosionDivide.p"),Gdx.files.internal("effects/"));
        sandstorm.load(Gdx.files.internal("effects/sandstorm.p"),Gdx.files.internal("effects/"));
        sandstorm.setPosition(settings.getWorldWidth(),settings.getWorldHeight());

        // update font
        font = new BitmapFont(Gdx.files.internal("font/font.txt"));
        font.setScale(0.8f);
    }

    /**
     * Disposes the particle effects.
     */
    public void disposeParticles(){
        if(explosionPlus != null){
            explosionPlus.dispose();
            explosionPlus = null;
        }
        if(explosionMinus != null){
            explosionMinus.dispose();
            explosionMinus = null;
        }
        if(explosionMultiply != null){
            explosionMultiply.dispose();
            explosionMultiply = null;
        }
        if(explosionDivide != null){
            explosionDivide.dispose();
            explosionDivide = null;
        }
        if(sandstorm != null){
            sandstorm.dispose();
            sandstorm = null;
        }
        if(font != null){
            font.dispose();
            font = null;
        }

    }

    /**
     * Initializes the explosions for the correct calculations.
     *
     * @param number The current calculation
     */
    public void explosion(short number){

        float xpos = (settings.getWorldWidth() / 4);
        float ypos = settings.getWorldHeight() - settings.getWorldHeight() / 7;


        if(number == 1){
            if(explosionPlus.isComplete()){
                explosionPlus.reset();
                explosionPlus.setPosition(xpos,ypos);
                explosionPlus.start();
            }
        }
        else if(number == 2){
            if(explosionMinus.isComplete()){
                explosionMinus.reset();
                explosionMinus.setPosition(xpos,ypos);
                explosionMinus.start();
            }
        }
        else if(number == 3){
            if(explosionMultiply.isComplete()){
                explosionMultiply.reset();
                explosionMultiply.setPosition(xpos,ypos);
                explosionMultiply.start();
            }
        }
        else if(number == 4){
            if(explosionDivide.isComplete()){
                explosionDivide.reset();
                explosionDivide.setPosition(xpos,ypos);
                explosionDivide.start();
            }
        }
    }

    /**
     * Draws the explosions.
     * @param delta
     */
    private void drawExplosions(float delta){

        if(explosionPlus != null){
            if(!explosionPlus.isComplete()){
                explosionPlus.update(delta);
                explosionPlus.draw(batch);
            }
        }
        if(explosionMinus != null){
            if(!explosionMinus.isComplete()){
                explosionMinus.update(delta);
                explosionMinus.draw(batch);
            }
        }
        if(explosionMultiply != null){
            if(!explosionMultiply.isComplete()){
                explosionMultiply.update(delta);
                explosionMultiply.draw(batch);
            }
        }
        if(explosionDivide != null){
            if(!explosionDivide.isComplete()){
                explosionDivide.update(delta);
                explosionDivide.draw(batch);
            }
        }


    }
    private void drawSandstorm(float delta){

        if(dududu && sandstorm != null){
            sandstorm.update(delta);
            sandstorm.draw(batch);
        }
    }



}
