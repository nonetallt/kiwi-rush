package tamk.game.runnergame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import tamk.game.runnergame.Settings;

import java.util.ArrayList;


public class TutorialScreen implements Screen {

    private final String TAG = "TutorialScreen";
    private Settings settings;

    private boolean backPressed = false;
    private boolean forwardPressed = false;

    // textures
    private Texture backButton;
    private Texture backButtonPressed;
    private Texture forwardButton;
    private Texture forwardButtonPressed;

    // background images
    private int currentImageNumber = 1;
    private final int imageCount = 3;
    private boolean languageIsFinnish = false;
    private boolean languageChosen = false;
    private Texture background;
    private Texture fi1;
    private Texture fi2;
    private Texture fi3;
    private Texture en1;
    private Texture en2;
    private Texture en3;
    private BitmapFont defaultFont;
    // ...

    // camera
    private OrthographicCamera camera;
    // batch
    private SpriteBatch batch;

    // const
    public TutorialScreen(Settings settings){
        this.settings = settings;

        batch = settings.getBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, settings.getWorldWidth(), settings.getWorldHeight());

        updateTextures();
    }

    @Override
    public void render(float delta){

        batch.setProjectionMatrix(camera.combined);
        // buffer bit
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // back button on android
        androidBackInput();

        // get touch input for buttons
        touchInput();

        // drawing
        batch.begin();
        drawTutorialImage();
        drawBackButton();
        drawForwardButton();
        batch.end();

    }
    @Override
    public void show() {
        Gdx.app.log(TAG, "show()");
        updateTextures();
    }
    @Override
    public void resize(int width, int height) {
        Gdx.app.log(TAG, "resize width = " + width + ", height = " + height);

    }
    @Override
    public void pause() {
        Gdx.app.log(TAG, "pause()");
        settings.disposeAllAssets();
        defaultFont.dispose();
    }
    @Override
    public void resume() {
        settings.updateAllAssets();
        updateTextures();
        Gdx.app.log(TAG, "resume()");
    }
    @Override
    public void hide() {
        Gdx.app.log(TAG, "hide()");

    }
    @Override
    public void dispose() {
        Gdx.app.log(TAG, "dispose()");

    }

    /**
     * Gets the textures and the font for the tutorial.
     */
    private void updateTextures(){
        backButton = settings.getAssetManager().getTexture("backButton.png");
        backButtonPressed = settings.getAssetManager().getTexture("backButtonPressed.png");
        forwardButton = settings.getAssetManager().getTexture("forwardButton.png");
        forwardButtonPressed = settings.getAssetManager().getTexture("forwardButtonPressed.png");

        en1 = settings.getAssetManager().getTexture("tutorial1eng.jpg");
        en2 = settings.getAssetManager().getTexture("tutorial2eng.jpg");
        en3 = settings.getAssetManager().getTexture("tutorial3eng.jpg");

        fi1 = settings.getAssetManager().getTexture("tutorial1fi.jpg");
        fi2 = settings.getAssetManager().getTexture("tutorial2fi.jpg");
        fi3 = settings.getAssetManager().getTexture("tutorial3fi.jpg");

        background = settings.getAssetManager().getTexture("highscoresBackground.jpg");

        // update font
        defaultFont = new BitmapFont(Gdx.files.internal("font/font.txt"));
        defaultFont.setScale(0.7f);
    }

    /**
     * Draws the back button.
     */
    private void drawBackButton(){
        if(!languageChosen){
            if(backPressed){
                batch.draw(backButtonPressed, settings.getWorldWidth() * 0.05f, settings.getWorldHeight() * 0.025f);
            }
            else{
                batch.draw(backButton, settings.getWorldWidth() * 0.05f, settings.getWorldHeight() * 0.025f);
            }
        }
    }

    /**
     * Draws the forward button.
     */
    private void drawForwardButton(){
        if(languageChosen && currentImageNumber == 3){
            if(forwardPressed){
                batch.draw(forwardButtonPressed, settings.getWorldWidth() - settings.getWorldWidth() * 0.05f -85, settings.getWorldHeight() * 0.025f);
            }
            else{
                batch.draw(forwardButton, settings.getWorldWidth() - settings.getWorldWidth() * 0.05f -85, settings.getWorldHeight() * 0.025f);
            }
        }
    }

    /**
     * Draws the tutorial graphics.
     */
    private void drawTutorialImage(){
        if(!languageChosen){
            batch.draw(background,0,0);

            float xPos = 75;
            float yPos = settings.getWorldHeight() - settings.getWorldHeight() / 2;
            defaultFont.draw(batch, "English", xPos, yPos );

            xPos = settings.getWorldWidth() - defaultFont.getBounds("Suomi").width -75;
            yPos = settings.getWorldHeight() - settings.getWorldHeight() / 2;
            defaultFont.draw(batch, "Suomi", xPos, yPos );
        }
        else{
            if(currentImageNumber == 1){
                if(languageIsFinnish){
                    batch.draw(fi1,0,0);
                }
                else{
                    batch.draw(en1,0,0);
                }
            }
            else if(currentImageNumber == 2){
                if(languageIsFinnish){
                    batch.draw(fi2,0,0);
                }
                else{
                    batch.draw(en2,0,0);
                }
            }
            else if(currentImageNumber == 3){

                if(languageIsFinnish){
                    batch.draw(fi3,0,0);
                }
                else{
                    batch.draw(en3,0,0);
                }
            }
        }

    }

    /**
     * Touch input for the tutorial.
     */
    private void touchInput(){
        if(Gdx.input.justTouched()){

            Vector3 touchPosition = new Vector3();
            touchPosition.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPosition);

            float width = settings.getWorldWidth();
            float height = settings.getWorldHeight();

            if((touchPosition.x >= width * 0.05f && touchPosition.x <= width * 0.05f + 85)
                    &&     (touchPosition.y >= height * 0.025f && touchPosition.y <= height * 0.025f + 70)
                    && !languageChosen){
                backPressed = true;
            }
            else if((touchPosition.x >= width - width * 0.05f -85 && touchPosition.x <= width - width * 0.05f)
                    &&     (touchPosition.y >= height * 0.025f && touchPosition.y <= height * 0.025f + 70)
                    && languageChosen && currentImageNumber == 3){
                forwardPressed = true;
            }
            else if(touchPosition.x < settings.getWorldWidth() /2 && !languageChosen){
                languageChosen = true;
                languageIsFinnish = false;
            }
            else if(touchPosition.x >= settings.getWorldWidth() /2 && !languageChosen){
                languageChosen = true;
                languageIsFinnish = true;
            }
            else if(currentImageNumber != 3 && languageChosen) {
                    forwardPressed = true;
            }

        }
        // when not touching, finger is lifted
        else if(!Gdx.input.isTouched()){
            if(backPressed){
                settings.setScreenMenu();

                backPressed = false;
                settings.setTutorialDone(true);
                currentImageNumber = 1;
                languageChosen = false;
            }
            if(forwardPressed){
                if(currentImageNumber < imageCount){
                    forwardPressed = false;
                    currentImageNumber++;
                }
                else{
                    // after last image of tutorial, set screen to menu on forward button
                    settings.setScreenMenu();
                    settings.setTutorialDone(true);
                    forwardPressed = false;
                    currentImageNumber = 1;
                    languageChosen = false;
                }

            }


        }
    }

    private void androidBackInput(){
        if (Gdx.input.isKeyPressed(Input.Keys.BACK) && !settings.getInputDelayManager().isActive()){
            settings.getInputDelayManager().startDelay();

            System.out.println("Android back button pressed.");
            settings.setScreenMenu();
            settings.setTutorialDone(true);
            currentImageNumber = 1;
            languageChosen = false;
        }
    }
}
