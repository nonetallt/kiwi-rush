package tamk.game.runnergame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import javafx.scene.effect.Effect;
import tamk.game.runnergame.MainGame;
import tamk.game.runnergame.Settings;



import java.util.ArrayList;
import java.util.Timer;


public class MenuScreen implements Screen{

    // debug
    private final String TAG = "MenuScreen";

    // camera & batch & stuff
    private OrthographicCamera camera;
    private BitmapFont font;
    private SpriteBatch batch;

    // settings & utilities
    private Settings settings;
    private boolean paused = false;

    // navigation
    private short choice = 0;
    private boolean backPressed = false;
    private boolean buttonHoldSound = false;

    // textures
    private Texture backgroundTexture;
    private Texture buttons;
    private Texture buttonPressedHTP;
    private Texture buttonPressedPlay;
    private Texture buttonPressedHighscores;

    private Texture soundOn;
    private Texture soundOnPressed;
    private Texture soundOff;
    private Texture soundOffPressed;

    private Texture backButton;
    private Texture backButtonPressed;


    // constructor
    public MenuScreen(Settings settings){

        this.settings = settings;

        // rendering
        batch = settings.getBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, settings.getWorldWidth(), settings.getWorldHeight());

        // initialize textures
        updateTextures();

        font = new BitmapFont();
        font.scale(2.0f);


    }

    // screen methods
    @Override
    public void render(float delta){

        // back button on android
        androidBackInput();


        // rendering while game is not paused
        if(!paused){
            batch.setProjectionMatrix(camera.combined);

            Gdx.gl.glClearColor(0, 0, 0, 1);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            // input
            if (Gdx.input.isKeyPressed(Input.Keys.SPACE) && choice == 1){
                settings.setScreenGame();
            }
            else if(Gdx.input.justTouched() && choice == 1){
                settings.setScreenGame();
            }

            // phone input
            if(Gdx.input.justTouched()){

                Vector3 touchPosition = new Vector3();
                touchPosition.set(Gdx.input.getX(), Gdx.input.getY(), 0);
                camera.unproject(touchPosition);

                float width = settings.getWorldWidth();
                float height = settings.getWorldHeight();

                if(touchPosition.x >= width * 0.225f && touchPosition.x <= width * 0.775f){

                    if(touchPosition.y >= height * 0.35 + 214 && touchPosition.y < height * 0.35 + 294){
                        if(settings.isTutorialDone()){
                            choice = 1;
                        }
                        else{
                            choice = 2;
                        }
                    }
                    else if(touchPosition.y >= height * 0.35 + 107 && touchPosition.y < height * 0.35 + 187){
                        choice = 2;
                    }
                    else if(touchPosition.y >= height * 0.35 && touchPosition.y < height * 0.35 + 80){
                        choice = 3;
                    }
                }
                else if((touchPosition.x >= width * 0.05f && touchPosition.x <= width * 0.05f + 85)
                &&     (touchPosition.y >= height * 0.025f && touchPosition.y <= height * 0.025f + 70) ){
                    backPressed = true;
                }
                else if((touchPosition.x >= width - width * 0.05f -85 && touchPosition.x <= width - width * 0.05f)
                        &&     (touchPosition.y >= height * 0.025f && touchPosition.y <= height * 0.025f + 70) ){
                    if(settings.isMuted()){
                        settings.setMute(false);
                        buttonHoldSound = true;
                    }
                    else{
                        settings.setMute(true);
                        buttonHoldSound = true;
                    }
                }
            }
            // when not touching, finger is lifted
            else if(!Gdx.input.isTouched()){
                if(choice == 1){
                    settings.setScreenGame();
                    // reset choice to prevent game from automatically restarting
                    choice = 0;
                }
                else if(choice == 2){
                    settings.setScreenTutorial();
                    // reset choice to prevent game from automatically restarting
                    choice = 0;
                }
                else if(choice == 3){
                    settings.setScreenHighscore();
                    // reset choice to prevent game from automatically restarting
                    choice = 0;
                }

                if(backPressed){
                    backPressed = false;
                    Gdx.app.exit();
                }
                if(buttonHoldSound){
                    buttonHoldSound = false;
                }
            }


            // drawing
            batch.begin();
            batch.draw(backgroundTexture,0,0);

            // menu buttons
            drawMenuButtons();

            // back button
            drawBackButton();

            // mute button
            drawMuteButton();


            batch.end();

        }
    }
    @Override
    public void show() {
        Gdx.app.log(TAG, "show()");

    }
    @Override
    public void resize(int width, int height) {
        Gdx.app.log(TAG, "resize width = " + width + ", height = " + height);

    }
    @Override
    public void pause() {
        settings.disposeAllAssets();
        Gdx.app.log(TAG, "pause()");
        paused = true;
    }
    @Override
    public void resume() {
        settings.updateAllAssets();
        updateTextures();

        Gdx.app.log(TAG, "resume()");
        paused = false;
    }
    @Override
    public void hide() {
        Gdx.app.log(TAG, "hide()");

    }
    @Override
    public void dispose() {
        Gdx.app.log(TAG, "dispose()");
    }

    /**
     * Draws the menu buttons.
     */
    private void drawMenuButtons(){
        if(choice == 0){
            batch.draw(buttons,settings.getWorldWidth() * 0.225f,settings.getWorldHeight() * 0.35f);
        }
        else if(choice == 1){
            batch.draw(buttonPressedPlay,settings.getWorldWidth() * 0.225f,settings.getWorldHeight() * 0.35f);
        }
        else if(choice == 2){
            batch.draw(buttonPressedHTP,settings.getWorldWidth() * 0.225f,settings.getWorldHeight() * 0.35f);
        }
        else if(choice == 3){
            batch.draw(buttonPressedHighscores,settings.getWorldWidth() * 0.225f,settings.getWorldHeight() * 0.35f);
        }
    }

    /**
     * Draws the back button.
     */
    private void drawBackButton(){
        if(backPressed){
            batch.draw(backButtonPressed, settings.getWorldWidth() * 0.05f, settings.getWorldHeight() * 0.025f);
        }
        else{
            batch.draw(backButton, settings.getWorldWidth() * 0.05f, settings.getWorldHeight() * 0.025f);
        }
    }

    /**
     * Draws the mute button.
     */
    private void drawMuteButton(){
        if(settings.isMuted()){
            if(!buttonHoldSound){
                batch.draw(soundOff, settings.getWorldWidth() - settings.getWorldWidth() * 0.05f -85, settings.getWorldHeight() * 0.025f);
            }
            else{
                batch.draw(soundOffPressed, settings.getWorldWidth() - settings.getWorldWidth() * 0.05f -85, settings.getWorldHeight() * 0.025f);
            }
        }
        else{
            if(!buttonHoldSound){
                batch.draw(soundOn, settings.getWorldWidth() - settings.getWorldWidth() * 0.05f -85, settings.getWorldHeight() * 0.025f);
            }
            else{
                batch.draw(soundOnPressed, settings.getWorldWidth() - settings.getWorldWidth() * 0.05f -85, settings.getWorldHeight() * 0.025f);
            }
        }
    }

    /**
     * Gets the textures and the font for the menus.
     */
    public void updateTextures(){
        backgroundTexture = settings.getAssetManager().getTexture("menuScreen.jpg");
        buttons = settings.getAssetManager().getTexture("menuButtons.png");
        buttonPressedHTP = settings.getAssetManager().getTexture("menuPressedHtp.png");
        buttonPressedPlay = settings.getAssetManager().getTexture("menuPressedPlay.png");
        buttonPressedHighscores = settings.getAssetManager().getTexture("menuPressedHighscores.png");
        backButton = settings.getAssetManager().getTexture("backButton.png");
        backButtonPressed = settings.getAssetManager().getTexture("backButtonPressed.png");
        soundOn = settings.getAssetManager().getTexture("soundOn.png");
        soundOnPressed = settings.getAssetManager().getTexture("soundOnPressed.png");
        soundOff = settings.getAssetManager().getTexture("soundOff.png");
        soundOffPressed = settings.getAssetManager().getTexture("soundOffPressed.png");
    }

    private void androidBackInput(){
        if (Gdx.input.isKeyPressed(Input.Keys.BACK) && !settings.getInputDelayManager().isActive()){

            System.out.println("Android back button pressed.");
            Gdx.app.exit();
        }
    }

}
