package tamk.game.runnergame.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import tamk.game.runnergame.Settings;

import java.util.ArrayList;


public class HighscoreScreen implements Screen {
    private final String TAG = "HighscoreScreen";
    private Settings settings;

    private boolean backPressed = false;

    // textures
    private Texture backButton;
    private Texture backButtonPressed;
    private Texture background;

    // camera
    private OrthographicCamera camera;
    // batch
    private SpriteBatch batch;

    // scores text
    private float namesFontX;
    private float namesFontY;
    private BitmapFont defaultFont;

    // const
    public HighscoreScreen(Settings settings){
        this.settings = settings;

        batch = settings.getBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, settings.getWorldWidth(), settings.getWorldHeight());

        updateTextures();
    }

    @Override
    public void render(float delta){

        batch.setProjectionMatrix(camera.combined);
        // buffer bit
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // back button on android
        androidBackInput();

        // get touch input for buttons
        touchInput();
        resetHighscores();

        // drawing
        batch.begin();
        batch.draw(background,0,0);
        drawNames();
        drawScores();
        drawBackButton();
        batch.end();

    }
    @Override
    public void show() {
        Gdx.app.log(TAG, "show()");
        updateTextures();
    }
    @Override
    public void resize(int width, int height) {
        Gdx.app.log(TAG, "resize width = " + width + ", height = " + height);

    }
    @Override
    public void pause() {
        Gdx.app.log(TAG, "pause()");
        defaultFont.dispose();
        settings.disposeAllAssets();
    }
    @Override
    public void resume() {
        Gdx.app.log(TAG, "resume()");
        settings.updateAllAssets();
        updateTextures();
    }
    @Override
    public void hide() {
        Gdx.app.log(TAG, "hide()");

    }
    @Override
    public void dispose() {
        Gdx.app.log(TAG, "dispose()");
        defaultFont.dispose();
    }

    /**
     * Gets the textures and the font for the highscores.
     */
    private void updateTextures(){
        backButton = settings.getAssetManager().getTexture("backButton.png");
        backButtonPressed = settings.getAssetManager().getTexture("backButtonPressed.png");
        background = settings.getAssetManager().getTexture("highscoresBackground.jpg");

        // update font
        defaultFont = new BitmapFont(Gdx.files.internal("font/font.txt"));
        defaultFont.setScale(0.5f);
    }

    /**
     * Draws the back button for the highscores.
     */
    private void drawBackButton(){
        if(backPressed){
            batch.draw(backButtonPressed, settings.getWorldWidth() * 0.05f, settings.getWorldHeight() * 0.025f);
        }
        else{
            batch.draw(backButton, settings.getWorldWidth() * 0.05f, settings.getWorldHeight() * 0.025f);
        }
    }

    /**
     * Draws the user-submitted names for the highscores
     */
    private void drawNames(){

        float startingHeight = settings.getWorldHeight() - settings.getWorldHeight()*0.1f;
        String[] names =   settings.getHighscoreNames();

        for(int n = 0; n < settings.getHighscoreNames().length; n++){

            //namesFontX = (settings.getWorldWidth() /2 - defaultFont.getBounds(n+1 + ". " + names[n]).width /2) / 2;
            namesFontX = settings.getWorldWidth() * 0.1f;
            namesFontY = startingHeight - defaultFont.getBounds(n+1 + ". " + names[n]).height * n * 2.75f;

            if(n == 9){
                defaultFont.draw(batch, n+1 + ". " + names[n] ,namesFontX - defaultFont.getBounds("0").width, namesFontY );
            }
            else{
                defaultFont.draw(batch, n+1 + ". " + names[n] ,namesFontX, namesFontY );
            }
        }
    }

    /**
     * Draws the scores for the highscores.
     */
    private void drawScores(){

        float startingHeight = settings.getWorldHeight() - settings.getWorldHeight()*0.1f;
        int[] values = settings.getHighscoreValues();


        for(int n = 0; n < settings.getHighscoreValues().length; n++){

            //namesFontX = (settings.getWorldWidth() /2 + defaultFont.getBounds(String.valueOf(values[n])).width /2);
            namesFontX = settings.getWorldWidth() * 0.7f;
            namesFontY = startingHeight - defaultFont.getBounds(String.valueOf(values[n])).height * n * 2.75f;

            defaultFont.draw(batch,String.valueOf(values[n]) ,namesFontX, namesFontY );
        }
    }

    /**
     * Touch input for the highscores.
     */
    private void touchInput(){
        if(Gdx.input.justTouched()){

            Vector3 touchPosition = new Vector3();
            touchPosition.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPosition);

            float width = settings.getWorldWidth();
            float height = settings.getWorldHeight();

            if((touchPosition.x >= width * 0.05f && touchPosition.x <= width * 0.05f + 85)
                    &&     (touchPosition.y >= height * 0.025f && touchPosition.y <= height * 0.025f + 70) ){
                backPressed = true;
            }

        }
        // when not touching, finger is lifted
        else if(!Gdx.input.isTouched()){
            if(backPressed){
                settings.setScreenMenu();
                backPressed = false;
            }
        }
    }

    // testing purposes, desktop only
    private void resetHighscores(){
        if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            settings.resetScores();
        }
    }

    private void androidBackInput(){
        if (Gdx.input.isKeyPressed(Input.Keys.BACK) && !settings.getInputDelayManager().isActive()){
            settings.getInputDelayManager().startDelay();

            System.out.println("Android back button pressed.");
            settings.setScreenMenu();
        }
    }
}
