package tamk.game.runnergame.levels;

/**
 * Contains data concerning obstacles.
 */
public class ObstacleData {

    boolean passable;
    boolean empty;

    // constructor
    public ObstacleData(boolean passable, boolean empty){
        this.passable = passable;
        this.empty = empty;
    }

    public boolean isPassable(){
        return passable;
    }
    public boolean isEmpty(){
        return empty;
    }
}
