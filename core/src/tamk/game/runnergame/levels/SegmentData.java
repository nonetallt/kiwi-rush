package tamk.game.runnergame.levels;

import tamk.game.runnergame.utilities.LevelReader;

/**
 * Contains data concerning level segments.
 */
public class SegmentData {

    private String name;
    private int difficulty;
    private int length;
    private ObstacleData[][] segmentData;

    // constructor
    public SegmentData(){
    }


    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getDifficulty() {
        return difficulty;
    }
    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }
    public int getLength() {
        return length;
    }
    public void setLength(int length) {
        this.length = length;
    }
    public ObstacleData[][] getSegmentData() {
        return segmentData;
    }
    public void setSegmentData(ObstacleData[][] segmentData) {
        this.segmentData = segmentData;
    }
}
