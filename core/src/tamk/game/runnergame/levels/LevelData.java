package tamk.game.runnergame.levels;

import tamk.game.runnergame.utilities.LevelReader;

/**
 * Contains data concerning levels.
 */
public class LevelData {

    private String name;
    private SegmentData[] segments;
    private int difficulty;
    private int length;

    // constructor
    public LevelData(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public SegmentData[] getSegments() {
        return segments;
    }

    public void setSegments(SegmentData[] segments) {
        this.segments = segments;
    }
}
