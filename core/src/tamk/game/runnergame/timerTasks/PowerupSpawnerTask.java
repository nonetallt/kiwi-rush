package tamk.game.runnergame.timerTasks;

import com.badlogic.gdx.utils.Timer;
import tamk.game.runnergame.Settings;
import tamk.game.runnergame.objects.Powerup;


public class PowerupSpawnerTask extends Timer.Task {

    private Settings settings;

    public PowerupSpawnerTask(Settings settings){
        this.settings = settings;
    }

    /**
     * Spawns the game's powerups randomly.
     */
    public void run(){

        // 5 = coin
        // 6 = health
        // 7 = Mushroom

        int random = settings.randomWithRange(1,100);

        if(settings.getCurrentLevel().getName().contains("forest")){

            if(random > 100 - (settings.getCombo() /2) && settings.getCombo() > 1 && settings.getGameScreen().getScoreManager().getScore() > 1000 && !settings.getGameScreen().isDrawingTransition()){
                settings.getObjects().add(new Powerup("1widthObstacleMushroom.png",settings,7));
            }
            else if(random > 100 - (settings.getCombo() * 2)  && settings.getPlayer().getHealth() < 5){
                settings.getObjects().add(new Powerup("healthTexture.png",settings,6));
            }
            else{
                settings.getObjects().add(new Powerup("scoreTexture.png",settings,5));
            }
        }
        else if(settings.getCurrentLevel().getName().contains("desert")){
            if(random > 100 - (settings.getCombo() * 1)  && settings.getPlayer().getHealth() < 5){
                settings.getObjects().add(new Powerup("healthTexture.png",settings,6));
            }
        }

        settings.getGameScreen().resetPowerupTime();
    }

}
