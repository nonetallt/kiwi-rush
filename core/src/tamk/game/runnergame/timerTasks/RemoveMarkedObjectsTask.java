package tamk.game.runnergame.timerTasks;

import com.badlogic.gdx.utils.Timer;
import tamk.game.runnergame.Settings;
import tamk.game.runnergame.objects.GameObject;

import java.util.ArrayList;
import java.util.Iterator;


public class RemoveMarkedObjectsTask extends Timer.Task{

    private ArrayList<GameObject> gameObjects;
    private Settings settings;

    // constructor
    public RemoveMarkedObjectsTask(Settings settings){
        this.settings = settings;
    }

    /**
     * Removes unnecessary objects from the game world through an iterator.
     */
    public void run(){
        System.out.println("Starting marked object removal: ");

        gameObjects = settings.getObjects();
        Iterator<GameObject> itr = gameObjects.iterator();

        while (itr.hasNext()) { // list iterator to avoid concurrency errors with main code
            GameObject object = itr.next();

            if(object.isMarkedForRemoval()){
                //System.out.println("-" + object + " removed. Reason: marked for removal.");
                itr.remove();
            }

        }
    }
}
