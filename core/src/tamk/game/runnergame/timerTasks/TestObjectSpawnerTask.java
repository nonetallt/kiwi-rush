package tamk.game.runnergame.timerTasks;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Timer;
import tamk.game.runnergame.Settings;
import tamk.game.runnergame.levels.LevelData;
import tamk.game.runnergame.levels.ObstacleData;
import tamk.game.runnergame.levels.SegmentData;
import tamk.game.runnergame.objects.GameObject;
import tamk.game.runnergame.objects.ObstacleTest;
import tamk.game.runnergame.objects.Powerup;

import java.util.ArrayList;
import java.util.Iterator;


public class TestObjectSpawnerTask extends Timer.Task{

    private Settings settings;
    private ArrayList<GameObject> gameObjects;

    // levelStuff
    private int segmentsLeft;
    private int obstaclesLeft;
    private LevelData level;
    private SegmentData[] segmentData;
    private SegmentData currentSegment;
    private int spawnBuffer;

    // obstacle data
    private String obstacle1S;
    private String obstacle2S;
    private String obstacle3S;

    private String obstacle1D;
    private String obstacle2D;


    // constructor
    public TestObjectSpawnerTask(Settings settings){

        this.settings = settings;
        gameObjects = settings.getObjects();

        initializeData();
        matchTexturesLevel();
    }

    // actual task
    public void run(){

        // if there are segments left in the level
        if(segmentsLeft > 0){

            gameObjects = settings.getObjects();
            ArrayList<GameObject> obstacles = new ArrayList<GameObject>();

            for(GameObject object : gameObjects){
                if(object.getType() >= 0 && object.getType() <= 3 ){
                    obstacles.add(object);
                }
            }

            if(obstacles.size() > 0){
                GameObject lastObject = obstacles.get(obstacles.size() -1); // last added object to obstacles
                // if last spawned object has gone lower than 160 px and there are obstacles left to spawn in segment
                if(lastObject.getY() + 160 * spawnBuffer <= settings.getWorldHeight() && obstaclesLeft > 0){

                    ObstacleData[][] dataTable = currentSegment.getSegmentData();
                    String tmp = combineSlots(dataTable, obstaclesLeft); // converts Obstacle array to string

                    //System.out.println("Reading: "+tmp);

                    translatePositions(tmp); // spawns correct obstacle

                    System.out.print("Obstacles left: " + obstaclesLeft);
                    System.out.println("  ---  Segments left: "+ segmentsLeft);

                }
                // if there are no obstacles to spawn, random a new segment from levels segment pool
                if(obstaclesLeft == 0){
                    segmentsLeft--;
                    randomSegment();
                }

            }

        }
        // if current level is completed, set a new level
        else{
            if(settings.getCurrentLevel().getName().contains("forest")){

                if(settings.randomWithRange(1,2) == 1){
                    if(settings.getGameScreen().getScoreManager().getScore() >= 100000){
                        settings.getGameScreen().setTransition(true);
                        settings.setCurrentLevel("DesertInferno");
                    }
                    else if(settings.getGameScreen().getScoreManager().getScore() >= 25000){
                        settings.getGameScreen().setTransition(true);
                        settings.setCurrentLevel("DesertHard");
                    }
                    else{
                        settings.getGameScreen().setTransition(true);
                        settings.setCurrentLevel("DesertEasy");
                    }
                }
                else{
                    if(settings.getGameScreen().getScoreManager().getScore() >= 100000){
                        settings.getGameScreen().setTransition(true);
                        settings.setCurrentLevel("ForestInferno");
                    }
                    else if(settings.getGameScreen().getScoreManager().getScore() >= 25000){
                        settings.setCurrentLevel("ForestHard");
                    }
                    else{
                        settings.setCurrentLevel("ForestEasy");
                    }
                }
            }
            else{
                if(settings.getCurrentLevel().getName().contains("desert")){
                    settings.getGameScreen().setTransition(true);
                }
                if(settings.getGameScreen().getScoreManager().getScore() >= 25000){
                    settings.setCurrentLevel("ForestHard");
                }
                else{
                    settings.setCurrentLevel("ForestEasy");
                }
            }
            randomSegment();
            segmentsLeft = settings.getCurrentLevel().getLength();
        }
    }

    // updates data
    private void randomSegment(){
        level = settings.getCurrentLevel();
        matchTexturesLevel();
        segmentData = level.getSegments();
        currentSegment = segmentData[settings.randomWithRange(0,segmentData.length -1)];
        obstaclesLeft = currentSegment.getLength();
    }
    // construct if no previous data exists
    public void initializeData(){

        if(level == null){
            level = settings.getCurrentLevel();
            segmentData = level.getSegments();
            currentSegment = segmentData[settings.randomWithRange(0,segmentData.length -1)];
            obstaclesLeft = currentSegment.getLength();

            segmentsLeft = level.getLength();
            spawnBuffer = 1;
        }
    }

    public void updateData(){
        randomSegment();
        segmentsLeft = settings.getCurrentLevel().getLength();
    }

    // get String data
    private String combineSlots(ObstacleData[][] data, int index){

        String tmp ="";

        for(int n = 0; n < 3; n++){

            if(data[index-1][n].isPassable() && !data[index-1][n].isEmpty()){
                tmp += "X";
            }
            else if(!data[index-1][n].isPassable() && !data[index-1][n].isEmpty()){
                tmp += "O";
            }
            else{
                tmp += "-";
            }

        }
        return tmp;
    }
    // spawn at correct locations
    private void translatePositions(String tmp){

            // for efficiency, check which list to search
            if(tmp.contains("X") && tmp.contains("O")){
                searchSpawnListBoth(tmp);
            }
            else if(tmp.contains("X")){
                searchSpawnListX(tmp);
            }
            else if(tmp.contains("O")){
                searchSpawnListO(tmp);
            }
            else{
                spawnBuffer++;
            }


        obstaclesLeft--;
    }

    // search for more efficiency
    private void searchSpawnListBoth(String tmp){
        spawnBuffer = 1;

        if(tmp.equalsIgnoreCase("X-O")) {
            gameObjects.add(new ObstacleTest(settings,1, 1, obstacle1S, true));
            gameObjects.add(new ObstacleTest(settings,3, 1, obstacle1D, false));
        }
        else if(tmp.equalsIgnoreCase("O-X")) {
            gameObjects.add(new ObstacleTest(settings,1, 1, obstacle1D, false));
            gameObjects.add(new ObstacleTest(settings,3, 1, obstacle1S, true));
        }
        else if(tmp.equalsIgnoreCase("XO-")) {
            gameObjects.add(new ObstacleTest(settings,1, 1, obstacle1S, true));
            gameObjects.add(new ObstacleTest(settings,2, 1, obstacle1D, false));
        }
        else if(tmp.equalsIgnoreCase("-OX")) {
            gameObjects.add(new ObstacleTest(settings,2, 1, obstacle1D, false));
            gameObjects.add(new ObstacleTest(settings,3, 1, obstacle1S, true));
        }
        else if(tmp.equalsIgnoreCase("OX-")) {
            gameObjects.add(new ObstacleTest(settings,1, 1, obstacle1D, false));
            gameObjects.add(new ObstacleTest(settings,2, 1, obstacle1S, true));
        }
        else if(tmp.equalsIgnoreCase("-XO")) {
            gameObjects.add(new ObstacleTest(settings,2, 1, obstacle1S, true));
            gameObjects.add(new ObstacleTest(settings,3, 1, obstacle1D, false));
        }
        else if(tmp.equalsIgnoreCase("XXO")) {
            gameObjects.add(new ObstacleTest(settings,1, 2, obstacle2S, true));
            gameObjects.add(new ObstacleTest(settings,3, 1, obstacle1D, false));
        }
        else if(tmp.equalsIgnoreCase("OXX")) {
            gameObjects.add(new ObstacleTest(settings,1, 1, obstacle1D, false));
            gameObjects.add(new ObstacleTest(settings,3, 2, obstacle2S, true));
        }
        else if(tmp.equalsIgnoreCase("OOX")) {
            gameObjects.add(new ObstacleTest(settings,1, 2, obstacle2D, false));
            gameObjects.add(new ObstacleTest(settings,3, 1, obstacle1S, true));
        }
        else if(tmp.equalsIgnoreCase("XOO")) {
            gameObjects.add(new ObstacleTest(settings,1, 1, obstacle1S, true));
            gameObjects.add(new ObstacleTest(settings,3, 2, obstacle2D, false));
        }
        else if(tmp.equalsIgnoreCase("XOX")) {
            gameObjects.add(new ObstacleTest(settings,1, 1, obstacle1S, true));
            gameObjects.add(new ObstacleTest(settings,2, 1, obstacle1D, false));
            gameObjects.add(new ObstacleTest(settings,3, 1, obstacle1S, true));
        }
        else if(tmp.equalsIgnoreCase("OXO")) {
            gameObjects.add(new ObstacleTest(settings,1, 1, obstacle1D, false));
            gameObjects.add(new ObstacleTest(settings,2, 1, obstacle1S, true));
            gameObjects.add(new ObstacleTest(settings,3, 1, obstacle1D, false));
        }


    }
    private void searchSpawnListX(String tmp){
        spawnBuffer = 1;
        if(tmp.equalsIgnoreCase("X--")) {
            gameObjects.add(new ObstacleTest(settings,1, 1, obstacle1S, true));
        }
        else if(tmp.equalsIgnoreCase("-X-")) {
            gameObjects.add(new ObstacleTest(settings,2, 1, obstacle1S, true));
        }
        else if(tmp.equalsIgnoreCase("--X")) {
            gameObjects.add(new ObstacleTest(settings,3, 1, obstacle1S, true));
        }
        else if(tmp.equalsIgnoreCase("X-X")) {
            // hole in middle, not passable
            gameObjects.add(new ObstacleTest(settings, 1, 1, obstacle1S, true));
            gameObjects.add(new ObstacleTest(settings,3, 1, obstacle1S, true));
        }
        else if(tmp.equalsIgnoreCase("XX-")) {
            // 2 width obstacle on left, passable
            gameObjects.add(new ObstacleTest(settings,1, 2, obstacle2S, true));
        }
        else if(tmp.equalsIgnoreCase("-XX")) {
            // 2 width obstacle on right, passable
            gameObjects.add(new ObstacleTest(settings,3, 2, obstacle2S, true));
        }
        else if(tmp.equalsIgnoreCase("XXX")) {
            // 3 width obstacle, passable
            gameObjects.add(new ObstacleTest(settings,1, 3, obstacle3S, true));

        }
    }
    private void searchSpawnListO(String tmp){
        spawnBuffer = 1;
        if(tmp.equalsIgnoreCase("O--")) {
            gameObjects.add(new ObstacleTest(settings,1, 1, obstacle1D, false));
        }
        else if(tmp.equalsIgnoreCase("-O-")) {
            gameObjects.add(new ObstacleTest(settings,2, 1, obstacle1D, false));
        }
        else if(tmp.equalsIgnoreCase("--O")) {
            gameObjects.add(new ObstacleTest(settings,3, 1, obstacle1D, false));
        }
        else if(tmp.equalsIgnoreCase("O-O")) {
            // hole in middle, not passable
            gameObjects.add(new ObstacleTest(settings, 1, 1, obstacle1D, false));
            gameObjects.add(new ObstacleTest(settings,3, 1, obstacle1D, false));
        }
        else if(tmp.equalsIgnoreCase("OO-")) {
            // 2 width obstacle on left, passable
            gameObjects.add(new ObstacleTest(settings,1, 2, obstacle2D, false));
        }
        else if(tmp.equalsIgnoreCase("-OO")) {
            // 2 width obstacle on right, passable
            gameObjects.add(new ObstacleTest(settings,3, 2, obstacle2D, false));
        }
    }

    public void matchTexturesLevel(){

        if(settings.getCurrentLevel().getName().contains("forest")){
            obstacle1S = "1widthObstacle.png";
            obstacle2S = "2widthObstacle.png";
            obstacle3S = "3widthObstacle.png";

            obstacle1D = "1widthObstacleDangerous.png";
            obstacle2D = "2widthObstacleDangerous.png";
        }
        else if(settings.getCurrentLevel().getName().contains("mushroom")){
            obstacle1S = "1widthObstacleMushroom.png";
            obstacle2S = "2widthObstacleMushroom.png";
            obstacle3S = "3widthObstacleMushroom.png";

            obstacle1D = "1widthObstacleMushroomDangerous.png";
            obstacle2D = "2widthObstacleMushroomDangerous.png";
        }
        else if(settings.getCurrentLevel().getName().contains("desert")){
            obstacle1S = "1widthObstacleDesert.png";
            obstacle2S = "2widthObstacleDesert.png";
            obstacle3S = "3widthObstacleDesert.png";

            obstacle1D = "1widthObstacleDesertDangerous.png";
            obstacle2D = "2widthObstacleDesertDangerous.png";
        }
    }


}
