package tamk.game.runnergame.timerTasks;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Timer;
import tamk.game.runnergame.Settings;
import tamk.game.runnergame.objects.GameObject;
import tamk.game.runnergame.objects.ObstacleTest;
import tamk.game.runnergame.objects.Powerup;

import java.util.ArrayList;


public class ObjectCollisionCheckerTask extends Timer.Task{

    private Settings settings;
    private ArrayList<GameObject> gameObjects;
    private ArrayList<GameObject> activeObstacles;
    private ArrayList<GameObject> powerups;

    // constructor
    public ObjectCollisionCheckerTask(Settings settings){

        this.settings = settings;
        gameObjects = settings.getObjects();
        activeObstacles = new ArrayList<GameObject>();
        powerups = new ArrayList<GameObject>();
    }

    /**
     * Checks collisions between the player and the enemies & powerups.
     *
     */
    public void run(){

        activeObstacles.clear();


        for(GameObject object: gameObjects){ // collect objects
            if( object instanceof ObstacleTest && !object.isMarkedForRemoval()){
                activeObstacles.add(object);
            }
        }

        for(GameObject object: gameObjects){
            if(object instanceof Powerup && !object.isMarkedForRemoval()){
                powerups.add(object);
            }
        }

        checkCollisionPowerup();

        for(GameObject object: activeObstacles){
            if(!object.hasBeenCollidedWith()){
                Rectangle collisionbox = new Rectangle(object.getX(),object.getY(),object.getWidth(),0.01f);

                // check if math array has been initialized
                String[] mathArray = new String[2];
                if(settings.getMathData() != null){
                    mathArray = settings.getMathData();
                }
                else{
                    mathArray[0] = "0";
                    mathArray[1] = "";
                }

                // collision check
                if(collisionbox.overlaps(settings.getPlayer().getBoundingRectangle()) && !settings.getPlayer().getJumpManager().isOnAir())
                {
                    object.setCollidedStatus(true);

                    if( ((settings.getCombo() < 1) || !object.isPassable())  ){

                            System.out.println("Player collision with obstacle: "+ this);
                            settings.setCombo(0);

                            if(settings.getPlayer().getHealth() > 1){
                                settings.getPlayer().setHealth( (short)(settings.getPlayer().getHealth() -1) );
                                settings.getPlayer().getLimpManager().startLimping();
                            }
                            else{
                                settings.getPlayer().setHealth( (short)(settings.getPlayer().getHealth() -1) );
                                // check for death to prevent multiple animation starts after death
                                if(!settings.getPlayer().isDead()){
                                    settings.getPlayer().setAnimation((short)6);
                                    settings.getPlayer().setDead(true);
                                }
                            }
                    }
                    else{
                        settings.getPlayer().getJumpManager().jump();
                            if(settings.getCombo() > 0){
                                settings.setCombo(settings.getCombo() - 1);
                            }
                            if(settings.getPlayer().getRollManager().isRolling()){
                                settings.getPlayer().getRollManager().stopRolling();
                            }
                    }
                }
            }
        }

    }


    private void checkCollisionPowerup(){

        for(GameObject object: powerups){
            if(!object.hasBeenCollidedWith()){
                Rectangle collisionbox = new Rectangle(object.getX(),object.getY(),object.getWidth(),0.01f);

                if(collisionbox.overlaps(settings.getPlayer().getBoundingRectangle() ) ){
                    object.setCollidedStatus(true);
                    checkPowerup(object);
                }
            }
        }
    }


    private void checkPowerup(GameObject object){
        if(object.getType() == 5){
            settings.getObjects().remove(object);
            System.out.println("Coin collected");

            if(!settings.isMuted()){
                settings.getAssetManager().getSound("score").stop();
                settings.getAssetManager().getSound("score").play();
            }

            settings.getGameScreen().getScoreManager().setScore(settings.getGameScreen().getScoreManager().getScore() + 500);

        }
        else if(object.getType() == 6){

            if(settings.getPlayer().getHealth() < 5){

                settings.getObjects().remove(object);
                System.out.println("Health collected");

                settings.getPlayer().setHealth( (short)(settings.getPlayer().getHealth() +1) );
                if(!settings.isMuted()){
                    settings.getAssetManager().getSound("health").stop();
                    settings.getAssetManager().getSound("health").play();
                }
            }
        }
        else if(object.getType() == 7){
            settings.getObjects().remove(object);
            settings.getObjects().removeAll(powerups);

            System.out.println("Mushroom collected");

            if(!settings.isMuted()){
                settings.getAssetManager().getSound("mushroom").stop();
                settings.getAssetManager().getSound("mushroom").play();
            }
            settings.setCombo(1);
            if(settings.getGameScreen().getScoreManager().getScore() >= 100000){
                settings.getGameScreen().setTransition(true);
                settings.setCurrentLevel("MushroomInferno");
            }
            else if(settings.getGameScreen().getScoreManager().getScore() >= 25000){
                settings.setCurrentLevel("MushroomHard");
            }
            else{
                settings.setCurrentLevel("MushroomEasy");
            }

        }
    }

}
