package tamk.game.runnergame.timerTasks;

import com.badlogic.gdx.utils.Timer;
import tamk.game.runnergame.Settings;

import java.util.Set;


public class LimpTimerTask extends Timer.Task{

    private Settings settings;

    // constructor
    public LimpTimerTask(Settings settings){
        this.settings = settings;
    }

    public void run(){
        settings.getPlayer().getLimpManager().stopLimping();
        settings.getGameScreen().stopLimpTime();
    }

}
