package tamk.game.runnergame.timerTasks;

import com.badlogic.gdx.utils.Timer;
import tamk.game.runnergame.Settings;


public class RollTimerTask extends Timer.Task{

    private Settings settings;

    // constructor
    public RollTimerTask(Settings settings){
        this.settings = settings;
    }

    public void run(){
        settings.getPlayer().getRollManager().stopRolling();
        settings.getGameScreen().stopRunningTime();
    }
}
