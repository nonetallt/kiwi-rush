package tamk.game.runnergame.timerTasks;

import com.badlogic.gdx.utils.Timer;
import tamk.game.runnergame.Settings;


public class ScoreTimerTask extends Timer.Task {

    private Settings settings;

    // constructor
    public ScoreTimerTask(Settings settings){
        this.settings = settings;
    }

    /**
     * Sets the player's score depending on the level.
     */
    public void run(){
        settings.getGameScreen().resetScoreTime();

        if(settings.getCurrentLevel().getName().contains("mushroom")){
            settings.getGameScreen().getScoreManager().setScore(settings.getGameScreen().getScoreManager().getScore()
                            + ( ((settings.getCombo()) +1) * 3)
            );
        }
        else if(settings.getCurrentLevel().getName().contains("desert")){
            settings.getGameScreen().getScoreManager().setScore(settings.getGameScreen().getScoreManager().getScore()
                            + ( ((settings.getCombo()) +1) * 2)
            );
        }
        else{
            settings.getGameScreen().getScoreManager().setScore(settings.getGameScreen().getScoreManager().getScore()
                            + ( settings.getCombo() ) +1
            );
        }
    }
}
