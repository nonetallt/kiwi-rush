package tamk.game.runnergame.timerTasks;

import com.badlogic.gdx.utils.Timer;
import tamk.game.runnergame.Settings;


public class DelayTask extends Timer.Task {

    private Settings settings;

    public DelayTask(Settings settings){
        this.settings = settings;
    }

    public void run(){
        settings.getInputDelayManager().setActive(false);
    }
}
