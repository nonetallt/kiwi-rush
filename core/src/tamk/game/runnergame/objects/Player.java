package tamk.game.runnergame.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import tamk.game.runnergame.Settings;
import tamk.game.runnergame.utilities.JumpManager;
import tamk.game.runnergame.utilities.LimpManager;
import tamk.game.runnergame.utilities.RollManager;
import tamk.game.runnergame.utilities.TextInput;

/**
 * Contains player data, animations, movement.
 */
public class Player extends GameObject {

    private static Texture playerTexture;
    private  final static float movementSpeed = 175f;
    private final static float accelMulti = 50f;
    private final static float speedComboScaling = 8.5f;
    private Settings settings;
    private final static int type = 0;

    // animation
    private Animation kiwiAnimationRun;
    private Animation kiwiAnimationJump;
    private Animation kiwiAnimationFly;
    private Animation kiwiAnimationLand;
    private Animation kiwiAnimationRoll;
    private Animation kiwiAnimationDie;
    private Animation kiwiAnimationLimp;
    private Animation currentAnimation;
    private float stateTime = 0;

    // health
    private short health;
    boolean dead;
    // jump
    private JumpManager jumpManager;
    // roll
    private RollManager rollManager;
    // limp
    private LimpManager limpManager;


    /**
     * Initializes basic features for the player character.
     *
     * @param settings @see Settings
     */
    public Player(Settings settings){

        super("healthTexture.png", settings,0, type, true);
        this.settings = settings;

        health = 3;
        dead = false;
        setPosition(  (settings.getWorldWidth() - this.getWidth()) / 2, (settings.getWorldHeight() / 5) - (getHeight()/2)); // starting position

        jumpManager = new JumpManager(settings,this);
        rollManager = new RollManager(settings);
        limpManager = new LimpManager(settings);

        setSize(40,40);
        animations();
    }

    /**
     * Creates animations for the player character.
     */
    public void animations(){

        // run animation for kiwi
        Texture animationSheetKiwiRun = settings.getAssetManager().getTexture("kiwiRunningSheet.png");
        TextureRegion[][] tmp1 = TextureRegion.split(animationSheetKiwiRun, animationSheetKiwiRun.getWidth()/5, animationSheetKiwiRun.getHeight()/4);
        TextureRegion [] frames1 = settings.transformTo1D(tmp1, 5, 4, 1);
        kiwiAnimationRun = new Animation(1/20f,frames1);

        // jump animation for kiwi
        Texture animationSheetKiwiJump = settings.getAssetManager().getTexture("kiwiJumpingSheet.png");
        TextureRegion[][] tmp2 = TextureRegion.split(animationSheetKiwiJump, animationSheetKiwiJump.getWidth()/5, animationSheetKiwiJump.getHeight()/2);
        TextureRegion [] frames2 = settings.transformTo1D(tmp2, 5, 2, 0);
        kiwiAnimationJump = new Animation(1/20f,frames2);

        // flying animation for kiwi
        Texture animationSheetKiwiFly =  settings.getAssetManager().getTexture("kiwiFlyingSheet.png");
        TextureRegion[][] tmp3 = TextureRegion.split(animationSheetKiwiFly, animationSheetKiwiFly.getWidth()/4, animationSheetKiwiFly.getHeight());
        TextureRegion [] frames3 = settings.transformTo1D(tmp3, 4, 1, 0);
        kiwiAnimationFly = new Animation(1/20f,frames3);

        // jump animation for kiwi
        Texture animationSheetKiwiLand = settings.getAssetManager().getTexture("kiwiLandingSheet.png");
        TextureRegion[][] tmp4 = TextureRegion.split(animationSheetKiwiLand, animationSheetKiwiLand.getWidth()/5, animationSheetKiwiLand.getHeight()/3);
        TextureRegion [] frames4 = settings.transformTo1D(tmp4, 5, 3, 4);
        kiwiAnimationLand = new Animation(1/20f,frames4);

        kiwiAnimationRoll = settings.createAnimation(settings.getAssetManager().getTexture("kiwiRollingSheet.png"),(short)5,(short)5,(short)0,1/30f);
        kiwiAnimationDie = settings.createAnimation(settings.getAssetManager().getTexture("kiwiDyingSheet.png"),(short)4 ,(short)4, (short)3, 1/20f);
        kiwiAnimationLimp = settings.createAnimation(settings.getAssetManager().getTexture("kiwiLimpingSheet.png"),(short)5 ,(short)4, (short)0, 1/20f);

        currentAnimation = kiwiAnimationRun;

    }

    /**
     * Draws the player.
     */
    public void drawPlayer(){
        stateTime += Gdx.graphics.getDeltaTime();

        // changes animation after one ends
        // jump animation ending -> in air animation
        if(currentAnimation == kiwiAnimationJump && currentAnimation.isAnimationFinished(stateTime)){
            currentAnimation = kiwiAnimationFly;
            setAnimation((short)3);
        }
        // landing animation ending -> back to running animation
        else if(currentAnimation == kiwiAnimationLand && currentAnimation.isAnimationFinished(stateTime)){
            setAnimation((short)1);
        }

        // Is animation non-looping?
        // non looping animations
        if(currentAnimation == kiwiAnimationJump || currentAnimation == kiwiAnimationLand || currentAnimation == kiwiAnimationDie){
            settings.getBatch().draw(currentAnimation.getKeyFrame(stateTime, false), getX(), getY());
        }
        else{ // looping
            settings.getBatch().draw(currentAnimation.getKeyFrame(stateTime, true), getX(), getY());
        }

        // ripperoni
        if(currentAnimation == kiwiAnimationDie && currentAnimation.isAnimationFinished(stateTime)){
            // highscore checks
            highscores();
        }

    }

    /**
     * Moves the player using the accelerometer input.
     *
     * @param accel The X-axis value from the accelerometer of the device.
     */
    public void moveMobile(float accel){

        // accel values are negative because otherwise movement is in the opposite direction of the angle

        if(!rollManager.isRolling()){
            // if moving out of screen left
            if(getX() + (-accel * Gdx.graphics.getDeltaTime() * accelMulti) < settings.getWorldWidth()* 0.15f){
                setX(settings.getWorldWidth()* 0.15f);
            }
            // if moving out of screen right
            else if (getX() + (-accel * Gdx.graphics.getDeltaTime() * accelMulti) > (settings.getWorldWidth() * 0.85f) - getWidth()){
                setX( (settings.getWorldWidth()* 0.85f) - getWidth());
            }
            // if not move normally
            else{
                // positive
                if(accel < 0){
                    translateX( (-accel * accelMulti + (settings.getCombo() * speedComboScaling) ) * Gdx.graphics.getDeltaTime() );
                }
                // negative
                else{
                    translateX( (-accel * accelMulti - (settings.getCombo() * speedComboScaling) ) * Gdx.graphics.getDeltaTime() );
                }

            }
        }

    }

    /**
     * Moves the player right.
     */
    public void moveRight(){
        translateX( (movementSpeed + settings.getCombo() * 25) * Gdx.graphics.getDeltaTime());
        if(getX() > settings.getWorldWidth() - getWidth()){
            setX(settings.getWorldWidth() - getWidth());
        }
    }

    /**
     * Moves the player left.
     */
    public void moveLeft(){
        translateX( (-movementSpeed - settings.getCombo() * 25) * Gdx.graphics.getDeltaTime());
        if(getX() < 0){
            setX(0);
        }
    }

    // setters
    public void setAnimation(short animation){

        switch (animation){
            case 1:
                currentAnimation = kiwiAnimationRun;
                break;
            case 2:
                currentAnimation = kiwiAnimationJump;
                break;
            case 3:
                currentAnimation = kiwiAnimationFly;
                break;
            case 4:
                currentAnimation = kiwiAnimationLand;
                break;
            case 5:
                currentAnimation = kiwiAnimationRoll;
                break;
            case 6:
                currentAnimation = kiwiAnimationDie;
                break;
            case 7:
                currentAnimation = kiwiAnimationLimp;
                break;
        }

        stateTime = 0;
    }
    public void checkLandingAnimation(){
        if(currentAnimation == kiwiAnimationFly){
            currentAnimation = kiwiAnimationLand;
        }
    }
    public void setAnimationSpeed(float speed){
        currentAnimation.setFrameDuration(speed);
    }
    public void setHealth(short health) {
        this.health = health;
    }
    public void setDead(boolean isDead){
        dead = isDead;
    }

    // getters
    public JumpManager getJumpManager(){
        return jumpManager;
    }
    public RollManager getRollManager() {
        return rollManager;
    }
    public short getHealth() {
        return health;
    }
    public boolean isDead(){
        return dead;
    }
    public LimpManager getLimpManager() {
        return limpManager;
    }

    /**
     * Checks for the highscores when the game ends.
     */
    private void highscores(){

        if(settings.getGameScreen().getScoreManager().checkForHighscore()){

            int score = settings.getGameScreen().getScoreManager().getScore();

            TextInput listener = settings.getListener();

            settings.resetGameData();
            settings.getAssetManager().initializeTextures();
            settings.setScreenHighscore();
            listener.setScoreData(score);

            Gdx.input.getTextInput(listener, "New highscore!", "", "-Please enter your name-");
        }
        else{
            settings.setScreenMenu();
        }
    }
}
