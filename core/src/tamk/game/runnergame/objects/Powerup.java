package tamk.game.runnergame.objects;

import tamk.game.runnergame.Settings;

/**
 * Contains data concerning powerups.
 */
public class Powerup extends GameObject{

    private final static boolean passable = false;

    public Powerup(String textureData, Settings settings,int type){
        super(textureData,settings,0, type, passable);

        setSize(64,64);

        // starting position
        setPosition(settings.randomWithRange(72,344), settings.getWorldHeight());
    }


}
