package tamk.game.runnergame.objects;

import tamk.game.runnergame.Settings;


public class ObstacleTest extends GameObject{

    private char type;

    // constructor
    public ObstacleTest(Settings settings, int laneNumber, int type, String textureData, boolean passable){
        super(textureData,settings,laneNumber, type, passable);

        // starting size for hit box
        // type, width

        switch (type){
            case 1: setSize(112f,112f);
                break;
            case 2: setSize(224f,112f);
                break;
            case 3: setSize(336f,112f);
                break;
        }

        // starting position

        switch (laneNumber){
            case 1: setPosition(settings.getWorldWidth() * 0.15f, settings.getWorldHeight());
                // lane 1
                break;
            case 2: setPosition( (settings.getWorldWidth() / 2) - (getWidth() /2 ), settings.getWorldHeight());
                // lane 2
                break;
            case 3: setPosition(settings.getWorldWidth() * 0.85f -getWidth(), settings.getWorldHeight());
                // lane 3
                break;
        }
    }


}
