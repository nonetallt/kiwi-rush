package tamk.game.runnergame.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import tamk.game.runnergame.Settings;

/**
 * Contains data concerning game objects.
 */
public class GameObject extends Sprite {

    private int laneNumber;
    private Settings settings;

    private boolean markedForRemoval = false;
    private boolean collidedStatus = false;
    private boolean passable;

    private String textureData;
    private int type;

    // constructor
    public GameObject(String textureData, Settings settings, int laneNumber, int type, boolean passable){

        // to move texture to sprite constructor
        super(settings.getAssetManager().getTexture(textureData));

        this.passable = passable;
        this.textureData = textureData;
        this.settings = settings;
        this.laneNumber = laneNumber;
        this.type = type;

    }

    /**
     * Moves objects relative to their lanes.
     */
    public void moveDown(){

        if(getY() >= -0.2f * settings.getWorldHeight()){ // if not below screen (0 - height of object)

            float movementY;

            movementY = -settings.getSpeed() * Gdx.graphics.getDeltaTime();
            translate(0, movementY);
        }
        // if moved below screen, mark for removal once
        else if(!markedForRemoval){
            markedForRemoval = true;
        }

    }

    // getters
    public boolean isMarkedForRemoval() {
        return markedForRemoval;
    }
    public boolean hasBeenCollidedWith(){
        return collidedStatus;
    }
    public int getType(){
        return type;
    }
    public boolean isPassable() {
        return passable;
    }

    // setters
    public void setCollidedStatus(boolean status){
        collidedStatus = status;
    }
    public String getTextureData() {
        return textureData;
    }
}
