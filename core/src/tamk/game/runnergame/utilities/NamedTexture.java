package tamk.game.runnergame.utilities;

import com.badlogic.gdx.graphics.Texture;


public class NamedTexture extends Texture{

    private String name;

    // constructor
    public NamedTexture(String textureName){
        super(textureName);
        name = textureName;
    }

    public String getName(){
        return name;
    }
}
