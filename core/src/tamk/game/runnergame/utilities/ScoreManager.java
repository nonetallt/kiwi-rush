package tamk.game.runnergame.utilities;

import com.badlogic.gdx.utils.Timer;
import tamk.game.runnergame.Settings;
import tamk.game.runnergame.timerTasks.ScoreTimerTask;


public class ScoreManager {

    private Settings settings;
    private Timer scoreTimer;
    private int score = 0;

    // constructor
    public ScoreManager(Settings settings){
        this.settings = settings;
        scoreTimer = new Timer();
    }

    public void startTimer(){
        settings.getGameScreen().resetScoreTime();
        scoreTimer.scheduleTask(new ScoreTimerTask(settings),0 ,0.1f);
    }

    /**
     * Checks if the current score is eligible for a highscore.
     *
     * @return Returns a truth value if a score is higher than the lowest highscore.
     */
    public boolean checkForHighscore(){
        int[] scores = settings.getHighscoreValues();
        int lowestScore = scores[0];

        for(int n = 0; n < scores.length; n++){
            if(scores[n] < lowestScore){
                 lowestScore = scores[n];
            }
        }
        if(score > lowestScore){
            return true;
        }
        else{
            return false;
        }
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Timer getScoreTimer(){
        return scoreTimer;
    }
}
