package tamk.game.runnergame.utilities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import tamk.game.runnergame.Settings;
import tamk.game.runnergame.objects.GameObject;

import java.util.ArrayList;

/**
 * Contains all the assets in the game.
 */
public class AssetManager {

    private Settings settings;
    private ArrayList<NamedTexture> textures;
    private Music currentMusic;
    private ArrayList<Sound> sounds;

    // constructor
    public AssetManager(Settings settings){
        this.settings = settings;
    }

    /**
     * Initializes all the textures in the game.
     */
    public void initializeTextures(){
        System.out.println("AssetManager: initializing textures.");

        textures = new ArrayList<NamedTexture>();

        // assets used in GameScreen
        textures.add(new NamedTexture("background.jpg"));
        textures.add(new NamedTexture("backgroundMushroom.jpg"));
        textures.add(new NamedTexture("backgroundDesert.jpg"));
        textures.add(new NamedTexture("background2top.png"));
        textures.add(new NamedTexture("buttons.jpg"));
        textures.add(new NamedTexture("buttonsp.jpg"));
        textures.add(new NamedTexture("buttonsm.jpg"));
        textures.add(new NamedTexture("buttonsx.jpg"));
        textures.add(new NamedTexture("buttonsx.jpg"));
        textures.add(new NamedTexture("buttonsd.jpg"));
        textures.add(new NamedTexture("healthTexture.png"));
        textures.add(new NamedTexture("scoreTexture.png"));
        textures.add(new NamedTexture("statusbar.jpg"));
        textures.add(new NamedTexture("forest-desert.jpg"));
        textures.add(new NamedTexture("desert-forest.jpg"));

        // assets used in MenuScreen
        textures.add(new NamedTexture("menuScreen.jpg"));
        textures.add(new NamedTexture("menuButtons.png"));
        textures.add(new NamedTexture("menuPressedHtp.png"));
        textures.add(new NamedTexture("menuPressedPlay.png"));
        textures.add(new NamedTexture("menuPressedHighscores.png"));
        textures.add(new NamedTexture("backButton.png"));
        textures.add(new NamedTexture("backButtonPressed.png"));
        textures.add(new NamedTexture("soundOn.png"));
        textures.add(new NamedTexture("soundOnPressed.png"));
        textures.add(new NamedTexture("soundOff.png"));
        textures.add(new NamedTexture("soundOffPressed.png"));

        // assets used in tutorial
        textures.add(new NamedTexture("forwardButton.png"));
        textures.add(new NamedTexture("forwardButtonPressed.png"));
        textures.add(new NamedTexture("tutorial1eng.jpg"));
        textures.add(new NamedTexture("tutorial2eng.jpg"));
        textures.add(new NamedTexture("tutorial3eng.jpg"));
        textures.add(new NamedTexture("tutorial1fi.jpg"));
        textures.add(new NamedTexture("tutorial2fi.jpg"));
        textures.add(new NamedTexture("tutorial3fi.jpg"));

        // assets used in objectSpawner
        textures.add(new NamedTexture("1widthObstacle.png"));
        textures.add(new NamedTexture("2widthObstacle.png"));
        textures.add(new NamedTexture("3widthObstacle.png"));
        textures.add(new NamedTexture("1widthObstacleDangerous.png"));
        textures.add(new NamedTexture("2widthObstacleDangerous.png"));

        textures.add(new NamedTexture("1widthObstacleMushroom.png"));
        textures.add(new NamedTexture("2widthObstacleMushroom.png"));
        textures.add(new NamedTexture("3widthObstacleMushroom.png"));
        textures.add(new NamedTexture("1widthObstacleMushroomDangerous.png"));
        textures.add(new NamedTexture("2widthObstacleMushroomDangerous.png"));

        textures.add(new NamedTexture("1widthObstacleDesert.png"));
        textures.add(new NamedTexture("2widthObstacleDesert.png"));
        textures.add(new NamedTexture("3widthObstacleDesert.png"));
        textures.add(new NamedTexture("1widthObstacleDesertDangerous.png"));
        textures.add(new NamedTexture("2widthObstacleDesertDangerous.png"));

        // assets used in highscores
        textures.add(new NamedTexture("highscoresBackground.jpg"));

        // player
        textures.add(new NamedTexture("kiwiRunningSheet.png"));
        textures.add(new NamedTexture("kiwiJumpingSheet.png"));
        textures.add(new NamedTexture("kiwiFlyingSheet.png"));
        textures.add(new NamedTexture("kiwiLandingSheet.png"));
        textures.add(new NamedTexture("kiwiRollingSheet.png"));
        textures.add(new NamedTexture("kiwiDyingSheet.png"));
        textures.add(new NamedTexture("kiwiLimpingSheet.png"));

    }

    /**
     * Initializes all the sounds in the game.
     */
    public void initializeAllSounds(){
        System.out.println("AssetManager: initializing sounds.");
        sounds = new ArrayList<Sound>();

        sounds.add(Gdx.audio.newSound(Gdx.files.internal("sound/runningSound.wav")));
        sounds.add(Gdx.audio.newSound(Gdx.files.internal("sound/jumpingSound.wav")));
        sounds.add(Gdx.audio.newSound(Gdx.files.internal("sound/fallingSound.wav")));
        sounds.add(Gdx.audio.newSound(Gdx.files.internal("sound/rollingSound.wav")));

        sounds.add(Gdx.audio.newSound(Gdx.files.internal("sound/scoreSound.mp3")));
        sounds.add(Gdx.audio.newSound(Gdx.files.internal("sound/healthSound.wav")));
        sounds.add(Gdx.audio.newSound(Gdx.files.internal("sound/mushroomSound.mp3")));
    }

    /**
     * Initializes all the music in the game.
     */
    public void initializeMusic(){

        System.out.println("AssetManager: disposing old music.");
        // dispose all music
        if(currentMusic != null){
            currentMusic.dispose();
            currentMusic = null;
        }

        System.out.println("AssetManager: initializing music.");

        if(settings.getCurrentLevel().getName().contains("forest")){
            currentMusic = Gdx.audio.newMusic(Gdx.files.internal("music/Beauceful Coppice.mp3"));
        }
        else if(settings.getCurrentLevel().getName().contains("mushroom")){
            currentMusic = Gdx.audio.newMusic(Gdx.files.internal("music/Mushroom Forest.mp3"));
        }
        else if(settings.getCurrentLevel().getName().contains("desert")){
            currentMusic = Gdx.audio.newMusic(Gdx.files.internal("music/Desert Slide.mp3"));
        }
    }

    public Texture getTexture(String textureName){

        // default texture if nothing is found
        Texture temp = null;

        // search texture
        for(NamedTexture texture: textures){
            if(texture.getName().equalsIgnoreCase(textureName)){
                temp = texture;
            }
        }

        return temp;
    }
    public Music getMusic(){
        return currentMusic;
    }
    public Sound getSound(String name){
        if(name.contains("running")){
            return sounds.get(0);
        }
        else if(name.contains("jumping")){
            return sounds.get(1);
        }
        else if(name.contains("falling")){
            return sounds.get(2);
        }
        else if(name.contains("rolling")){
            return sounds.get(3);
        }
        else if(name.contains("score")){
            return sounds.get(4);
        }
        else if(name.contains("health")){
            return sounds.get(5);
        }
        else if(name.contains("mushroom")){
            return sounds.get(6);
        }
        else{
            return sounds.get(0);
        }
    }

    public void dispose(){

        System.out.println("AssetManager: disposing textures.");

        // dispose all textures
        for(NamedTexture texture: textures){
            texture.dispose();
        }
        System.out.println("AssetManager: disposing music.");
        // dispose all musics
        if(currentMusic != null){
            currentMusic.dispose();
        }

        System.out.println("AssetManager: disposing sounds.");
        // dispose all sounds
        for(Sound sound: sounds){
            sound.dispose();
        }
        currentMusic = null;
        sounds.clear();
        textures.clear();
    }
    public void updateObjectTextures(){
        for(GameObject object : settings.getObjects()){
            object.setTexture(getTexture(object.getTextureData()));
        }
    }
    public void updateObjectTexturesLevelChange(){
        for(GameObject object : settings.getObjects()){

            if(settings.getCurrentLevel().getName().contains("forest")){

                if(object.isPassable()){
                    if(object.getType() == 1){
                        object.setTexture(getTexture("1widthObstacle.png"));
                    }
                    else if(object.getType() == 2){
                        object.setTexture(getTexture("2widthObstacle.png"));
                    }
                    else if(object.getType() == 3){
                        object.setTexture(getTexture("3widthObstacle.png"));
                    }
                }
                else{
                    if(object.getType() == 1){
                        object.setTexture(getTexture("1widthObstacleDangerous.png"));
                    }
                    else if(object.getType() == 2){
                        object.setTexture(getTexture("2widthObstacleDangerous.png"));
                    }
                }
            }
            else if(settings.getCurrentLevel().getName().contains("mushroom")){
                if(object.isPassable()){
                    if(object.getType() == 1){
                        object.setTexture(getTexture("1widthObstacleMushroom.png"));
                    }
                    else if(object.getType() == 2){
                        object.setTexture(getTexture("2widthObstacleMushroom.png"));
                    }
                    else if(object.getType() == 3){
                        object.setTexture(getTexture("3widthObstacleMushroom.png"));
                    }
                }
                else{
                    if(object.getType() == 1){
                        object.setTexture(getTexture("1widthObstacleMushroomDangerous.png"));
                    }
                    else if(object.getType() == 2){
                        object.setTexture(getTexture("2widthObstacleMushroomDangerous.png"));
                    }
                }
            }





        }
    }
}
