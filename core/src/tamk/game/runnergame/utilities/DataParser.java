package tamk.game.runnergame.utilities;


public class DataParser {

    private String[] stringArray;
    private int help;
    // const
    public DataParser(){
        help = 0;
    }

    public String[] parse(String data){

        String singleString = "";
        help = 0;
        data = data.replace(System.getProperty("line.separator"), ";");
        stringArray = new String[getLineCount(data)];

        for(int n = 0; n < data.length(); n++){

            String currentData = data.substring(n, n+1);

            if(currentData.contains(";") ){
                stringArray[help] = singleString;
                help ++;
                singleString = "";
            }
            else{

                singleString = singleString + data.substring(n , n+1);

            }
        }
        stringArray[help] = singleString;

        help ++;
        return stringArray;
    }
    private int getLineCount(String data){
        int tmp = 0;

        for(int n = 0; n < data.length(); n++){
            if(data.substring(n, n+1).contains(";")){
                tmp++;
            }
        }
        return tmp +1;
    }

}
