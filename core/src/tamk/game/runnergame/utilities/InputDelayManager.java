package tamk.game.runnergame.utilities;

import com.badlogic.gdx.utils.Timer;
import tamk.game.runnergame.Settings;
import tamk.game.runnergame.timerTasks.DelayTask;


public class InputDelayManager{

    private Timer delayTimer;
    private boolean active;
    private Settings settings;

    public InputDelayManager(Settings settings){
        delayTimer = new Timer();
        this.settings = settings;
    }
    public void startDelay(){
        active = true;
        delayTimer.scheduleTask(new DelayTask(settings),1);
    }
    public void setActive(boolean active){
        delayTimer.clear();
        this.active = active;
    }
    public boolean isActive() {
        return active;
    }

}
