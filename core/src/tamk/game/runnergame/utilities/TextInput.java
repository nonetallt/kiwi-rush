package tamk.game.runnergame.utilities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import tamk.game.runnergame.Settings;


public class TextInput implements Input.TextInputListener {

    private String username;
    private final String TAG = "TextInput";
    private int score = 0;
    private Settings settings;
    private String suggestedName ="";

    public TextInput(Settings settings){
        this.settings = settings;
    }

    @Override
    public void input (String text) {

        Gdx.app.log(TAG, "username entered: "+ username);

        if(text.length() <= 15){
            username = text;
        }
        else {
            username = text.substring(0,15);
        }
        settings.setHighscore(score);
    }

    @Override
    public void canceled () {
        Gdx.app.log(TAG, "input canceled");
        username = "unknown";
        settings.setHighscore(score);
    }

    public void setScoreData(int score){
        this.score = score;
    }

    public String getUsername(){
        return username;
    }

    public String getSuggestedName() {
        return suggestedName;
    }

}
