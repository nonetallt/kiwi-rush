package tamk.game.runnergame.utilities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import tamk.game.runnergame.Settings;
import tamk.game.runnergame.objects.Player;


public class JumpManager {


    private Settings settings;
    private Sprite sprite;

    private boolean onAir = false;
    private boolean falling = false;
    private float riseSpeed = 400f;
    private float fallSpeed = 400f;
    private float maxHeight = 320f;

    // constructor
    public JumpManager(Settings settings, Sprite sprite){
        this.settings = settings;
        this.sprite = sprite;
    }

    /**
     * Manages player jumping actions.
     */
    public void gravityCheck(){

        // if jumping
        if(onAir){
            // if maximum height is not reached yet and going up
            if(sprite.getY() < maxHeight && !falling){
                sprite.translateY(riseSpeed * Gdx.graphics.getDeltaTime());
            }
            else{
                falling = true;

                // checks for landing animation 5 steps before ground
                if(sprite.getY() - fallSpeed *  Gdx.graphics.getDeltaTime() * 5 <= (settings.getWorldHeight() / 5) - (sprite.getHeight()/2)){
                    settings.getPlayer().checkLandingAnimation();
                }
                // if going "below" ground, stop falling & jumping and adjust position to ground
                if(sprite.getY() - fallSpeed *  Gdx.graphics.getDeltaTime() <= (settings.getWorldHeight() / 5) - (sprite.getHeight()/2)){

                    // position of "ground"
                    sprite.setY((settings.getWorldHeight() / 5) - (sprite.getHeight() / 2));
                    // landing
                    falling = false;
                    onAir = false;
                }
                // otherwise just keep falling down
                else{
                    sprite.translateY(-fallSpeed * Gdx.graphics.getDeltaTime());

                }

            }
        }


    }

    /**
     * Executes the jump for the player.
     */
    public void jump() {
        if (!onAir) {
            onAir = true;
            settings.getPlayer().setAnimation((short)2);
        }
    }

    // getters
    public boolean isOnAir() {
        return onAir;
    }
}
