package tamk.game.runnergame.utilities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import tamk.game.runnergame.levels.LevelData;
import tamk.game.runnergame.levels.ObstacleData;
import tamk.game.runnergame.levels.SegmentData;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class LevelReader {

    private SegmentData segmentData;
    private SegmentData[] segments;
    private LevelData levelData;

    private int lengthHelpLevels;
    private int lengthHelpSegments;

    private final String filePath = "levels/";
    private FileHandle fileHandle;

    private ObstacleData[][] dataTable;

    private DataParser dataParser;

    public LevelReader(){
        lengthHelpSegments = 0;
        lengthHelpLevels = 0;
        dataParser = new DataParser();
    }

    /**
     * Reads a segment data file.
     *
     * @param fileName The name of the file.
     * @return Returns the read data from the file.
     */
    private SegmentData readSegmentData(String fileName){

        segmentData = null;
        segmentData = new SegmentData();
        dataTable = null;
        dataTable = new ObstacleData[checkLength(fileName, 6)][3];
        lengthHelpSegments = 0;
        fileName = fileName.replace(System.getProperty("line.separator"), "").replace("\r", "");

            String sCurrentLine;
            int lineNumber = 0;

            // testing
            fileHandle = Gdx.files.internal(filePath+fileName+".txt");
            String[] lines = dataParser.parse(fileHandle.readString());

            for(int n = 0; n < lines.length; n++){
                checkContentSegment(lines[n], lineNumber);
                lineNumber++;
            }

        // set collected values
        segmentData.setLength(lengthHelpSegments);
        segmentData.setSegmentData(dataTable);

        // return value
        return segmentData;
    }

    /**
     * Translates a segment data file into a level segment.
     *
     * @param line The line in the file that is being checked.
     * @param lineNumber The running number of the line.
     */
    private void checkContentSegment(String line, int lineNumber){
        if(lineNumber == 0){
            segmentData.setName(line.substring(6, line.length()));
        }
        else if(lineNumber == 1){
            segmentData.setDifficulty(Integer.valueOf(line.substring(12,13)));
        }
        else if(lineNumber > 4 && (line.contains("x") || line.contains("-")) ){
            // after 5th line, go through all characters in line, if character = x, slot is filled
            // check only 3 first characters because of android problems.
            for(int n = 0; n < 3; n++){

                if(line.substring(n, n+1).equalsIgnoreCase("X")){
                    dataTable[lengthHelpSegments][n] = new ObstacleData(true,false);
                }
                else if(line.substring(n, n+1).equalsIgnoreCase("O")){
                    dataTable[lengthHelpSegments][n] = new ObstacleData(false,false);
                }
                else{
                    dataTable[lengthHelpSegments][n] = new ObstacleData(true,true);
                }
            }
            lengthHelpSegments++;
        }
    }

    /**
     * Reads a level data file.
     *
     * @param fileName The name of the file.
     * @return Returns the read data from the file.
     */
    public LevelData readLevelData(String fileName){

        levelData = null;
        levelData = new LevelData();
        segments = null;
        segments = new SegmentData[checkLength(fileName, 5)];
        lengthHelpLevels = 0;
        fileName = fileName.replace(System.getProperty("line.separator"), "").replace("\r", "");

            String sCurrentLine;
            int lineNumber = 0;

            // testing
            fileHandle = Gdx.files.internal(filePath+fileName+".txt");
            String[] lines = dataParser.parse(fileHandle.readString());

            for(int n = 0; n < lines.length; n++){
                checkContentLevel(lines[n], lineNumber);
                lineNumber++;
            }

        // set collected values
        levelData.setSegments(segments);

        // return value
        return levelData;
    }

    /**
     * Checks the content of a certain line in a file.
     *
     * @param line The line in the file that is being checked.
     * @param lineNumber The running number of the line.
     */
    private void checkContentLevel(String line, int lineNumber){

        if(lineNumber == 0 && line.length() > 6){
            levelData.setName(line.substring(6));
        }
        else if(lineNumber == 1){
            levelData.setLength(Integer.valueOf(line.substring(8,9)));
        }
        else if(lineNumber > 3 && line.contains("segment")){
            segments[lengthHelpLevels] = readSegmentData(line);
            lengthHelpLevels++;
        }
    }

    /**
     * Checks the file length.
     *
     * @param fileName The name of the file.
     * @param startingLine The starting line.
     * @return Returns the length.
     */
    private int checkLength(String fileName, int startingLine){

        int length = 0;
        int help = 0;
        fileName = fileName.replace(System.getProperty("line.separator"), "").replace("\r", "");

            // testing
            fileHandle = Gdx.files.internal( (filePath+fileName+".txt") );
            String[] lines = dataParser.parse(fileHandle.readString());

            for(int n = 0; n < lines.length ;n++){
                help++;

                if(help >= startingLine){
                    length++;
                }
            }
        return length;
    }
}
