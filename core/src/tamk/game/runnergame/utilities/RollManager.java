package tamk.game.runnergame.utilities;

import com.badlogic.gdx.utils.Timer;
import tamk.game.runnergame.Settings;
import tamk.game.runnergame.timerTasks.RollTimerTask;


public class RollManager {

    private boolean rolling;
    private Settings settings;
    private Timer rollTimer;

    // constructor
    public RollManager(Settings settings){

        this.settings = settings;
        rolling = false;
        rollTimer = new Timer();
    }
    /**
     * Starts the player rolling.
     */
    public void startRolling(){
        // if not already rolling, start rolling.
        if(!rolling){
            rolling = true;

            // runs timer once
            rollTimer.scheduleTask(new RollTimerTask(settings),3);
            settings.getGameScreen().startRunningTime();
            settings.getPlayer().setAnimation((short)5);
        }
    }
    /**
     * Stops the player rolling.
     */
    public void stopRolling(){
        rolling = false;
        if(!settings.getPlayer().isDead()){
            settings.getPlayer().setAnimation((short)1);
        }

    }

    public boolean isRolling() {
        return rolling;
    }

    public Timer getRollTimer(){
        return rollTimer;
    }
}
