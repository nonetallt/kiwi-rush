package tamk.game.runnergame.utilities;

import com.badlogic.gdx.utils.Timer;
import tamk.game.runnergame.Settings;
import tamk.game.runnergame.timerTasks.LimpTimerTask;
import tamk.game.runnergame.timerTasks.RollTimerTask;


public class LimpManager {

    private boolean limping;
    private Settings settings;
    private Timer limpIimer;

    // constructor
    public LimpManager(Settings settings){

        this.settings = settings;
        limping = false;
        limpIimer = new Timer();
    }

    /**
     * Starts the player limping.
     */
    public void startLimping(){
        System.out.println("start");
        // if not already rolling, start rolling.
        if(!limping && !settings.getPlayer().getRollManager().isRolling()){
            limping = true;

            // runs timer once
            limpIimer.scheduleTask(new LimpTimerTask(settings),5);
            settings.getGameScreen().startLimpTime();
            settings.getPlayer().setAnimation((short)7);
        }
    }
    /**
     * Stops the player limping .
     */
    public void stopLimping(){
        System.out.println("stop");
        limping = false;
        if(!settings.getPlayer().isDead()){
            settings.getPlayer().setAnimation((short)1);
        }
    }

    public boolean isLimping() {
        return limping;
    }

    public Timer getLimpTimer(){
        return limpIimer;
    }
}
