package tamk.game.runnergame.utilities;

import com.badlogic.gdx.utils.Timer;
import tamk.game.runnergame.Settings;
import tamk.game.runnergame.timerTasks.PowerupSpawnerTask;
import tamk.game.runnergame.timerTasks.ScoreTimerTask;


public class PowerupManager {

    private Settings settings;
    private Timer spawnPowerupTimer;

    // constructor
    public PowerupManager(Settings settings){
        this.settings = settings;

        spawnPowerupTimer = new Timer();
    }

    public void startTimer(){
        settings.getGameScreen().resetScoreTime();
        spawnPowerupTimer.scheduleTask(new PowerupSpawnerTask(settings),10,3);
    }

    public Timer getSpawnPowerupTimer(){
        return spawnPowerupTimer;
    }

}
